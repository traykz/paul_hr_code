<?php

namespace App;


use App\Role;
use App\Profile;
use App\User;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function roles()
    {
        return $this->belongsToMany(Role::class)->withTimestamps();
    }


    public function authorizeRoles($roles)
{
    abort_unless($this->hasAnyRole($roles), 401);
    return true;
}


public function hasAnyRole($roles)
{
    if (is_array($roles)) {
        foreach ($roles as $role) {
            if ($this->hasRole($role)) {
                return true;
            }
        }
    } else {
        if ($this->hasRole($roles)) {
             return true;
        }
    }
    return false;
}


public function hasRole($role)
{
    if ($this->roles()->where('name', $role)->first()) {
        return true;
    }
    return false;
}


public function que_role($user_id)
{


        $rol = $this->roles()->where('user_id', $user_id)->first();

        return $rol->name;


}


public function ujefes(){

    //  return $this->hasMany('App\Historial', 'user_id', 'id', 'jefe_id');
     return $this->hasManyThrough('App\User',  'App\Historial', 'user_id', 'id', 'id', 'jefe_id');

}



public function departamentos()
{
        return $this->hasManyThrough('App\Departamento', 'App\Historial', 'user_id', 'id', 'id', 'departamento_id');
}


public function oficinas()
{
        return $this->hasManyThrough('App\Oficina', 'App\Historial', 'user_id', 'id', 'id', 'oficina_id');
}


public function puestos()
{
        return $this->hasManyThrough('App\Puesto', 'App\Historial', 'user_id', 'id', 'id', 'puesto_id');
}



public function profiledepartamentos()
{
        return $this->hasManyThrough('App\Departamento', 'App\Profile', 'user_id', 'id', 'id', 'departamento_id');
}


public function profileoficinas()
{
        return $this->hasManyThrough('App\Oficina', 'App\Profile', 'user_id', 'id', 'id', 'oficina_id');
}


public function profilepuestos()
{
        return $this->hasManyThrough('App\Puesto', 'App\Profile', 'user_id', 'id', 'id', 'puesto_id');
}


public function profilejefes(){

    //  return $this->hasMany('App\Historial', 'user_id', 'id', 'jefe_id');
     return $this->hasManyThrough('App\User',  'App\Profile', 'user_id', 'id', 'id', 'jefe_id');

}

public function fechas(){

    //  return $this->hasMany('App\Historial', 'user_id', 'id', 'jefe_id');
     return $this->hasMany('App\Historial', 'user_id', 'id');

}



public function zalario(){

    //  return $this->hasMany('App\Historial', 'user_id', 'id', 'jefe_id');
     return $this->hasMany('App\Profile', 'user_id', 'id');

}



public function salidas(){
    return $this->hasMany('App\Leave_user');
}



public function uLeaves(){
    return $this->hasManyThrough('App\Leave', 'App\Leave_user', 'user_id', 'id', 'id', 'leave_id');
}











}
