<?php

namespace App;

use App\Oficina;

use App\Departamento;

use App\Puesto;

use App\User;

use Illuminate\Database\Eloquent\Model;

use DB;

class Historial extends Model
{
    protected $table = 'historials';

    protected $fillable = ['user_id', 'departamento_id', 'oficina_id', 'puesto_id', 'jefe_id'];


    public function users (){
        return $this->belongsTo('App\User',  'user_id', 'id');
    }


    public function oficina()
    {
        return $this->belongsTo('App\Oficina', 'oficina_id', 'id');
    }

    public function departamento()
    {
        return $this->belongsTo('App\Departamento', 'departamento_id', 'id');
    }


    public function puesto()
    {
        return $this->belongsTo('App\Puesto', 'puesto_id', 'id');
    }


    public function user (){
        return $this->belongsTo('App\User', 'id');
    }

    public function usuario(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }




    public function jefe (){
        return $this->belongsTo('App\User',  'jefe_id', 'id');
    }




public function niveles(){



}


  /*  public function hUsers(){
        return $this->hasMany('App\User', 'id');
    }

    public function hOficinas(){
        return $this->hasMany('App\Oficina', 'id');
    }

    public function hDepartamentos(){
        return $this->hasMany('App\Departamento', 'id');
    }

    public function hPuestos(){
        return $this->hasMany('App\Puesto', 'id');
    } */




}
