<?php

namespace App;
use App\Historial;
use App\Profile;
use App\Nivel;

use Illuminate\Database\Eloquent\Model;

class Puesto extends Model
{
    protected $table = 'puestos';

    public function pHistorial(){
        return $this->belongsTo('App\Historial');
    }

    public function pProfile(){
        return $this->belongsTo('App\Profile');
    }

    public function  pNivel(){
        return $this->belongsTo('App\Nivel', 'nivel_id', 'id');
    }




}
