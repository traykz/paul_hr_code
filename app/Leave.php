<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leave extends Model
{
    protected $table = 'leaves';
    protected $fillable = ['tipo', 'dias'];


    public function users()
    {
        return $this->belongsToMany('App\User', 'leave_users', 'user_id', 'leaves_id');
    }

    public function pLeaves(){
        return $this->belongsTo('App\Leave');
    }


}
