<?php

namespace App\Http\Controllers;

use Auth;
use App\Nivel;
use App\Role;
use App\User;
use App\Oficina;
use App\Departamento;
use App\Puesto;
use App\Leave;
use App\Historial;
use App\Profile;
use App\Leave_user;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Collection;



class EmployeesController extends Controller
{
    //

     public function __construct(){
         $this->middleware('auth');

     }


   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->user()->authorizeRoles(['admin']);
        $employees = User::with('profileoficinas')->with('profiledepartamentos')->with('profilepuestos')->with('roles')->with('profilejefes')->get();
        return view('employees.index')->with('employees',$employees);
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function ajaxRequest()
    {
        return view('ajaxRequest');
    }


    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function ajaxRequestPost(Request $request)
    {
        $input = $request->all();
        return response()->json(['success'=>'Got Simple Ajax Request.']);
    }




    public function getNiveles(Request $request , $id){

        $nivel =  Nivel::with('puestos')->where('id', '=', $id)->get();
        $dropdown = '';
        foreach($nivel as $puestos){
           $dropdown = $puestos->puestos;
        }

        return response()->json($dropdown);

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {



        $request->user()->authorizeRoles(['admin']);

        $oficinas = Oficina::orderBy('nombre','asc')->get();
        $departmentos = Departamento::orderBy('nombre','asc')->get();
        $puestos    = Puesto::orderBy('nombre','asc')->get();
        $roles      = Role::get();
        $jefes      = Role::with('users')->where('name', '=', 'boss')->get();
        $nivel_salida = Leave::get();
        $nivel =    Nivel::with('puestos')->get();




       return view('employees.create')->with([
            'roles' => $roles,
            'oficinas' => $oficinas,
            'departamentos' => $departmentos,
            'puestos' => $puestos,
            'jefes' => $jefes,
            'nivel_salidas' => $nivel_salida,
            'niveles' => $nivel

            ]);
       }


        /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->user()->authorizeRoles(['admin']);

        /**
         *  validateRequest is a method defined in this controller
         *  which will validate  the form. we have created
         *  it so we can reuse it in the update method with
         *  different parameters.
         */
        $this->validateRequest_crear($request,null);

        /**
         *  Note!
         *  before using storage we need to link it to
         *  the public folder by typing the command,
         *  php artisan storage:link
         */

        /**
         *
         *  Handle the image file upload which will be stored
         *  in storage/app/public/employee_images
         */
        $fileNameToStore = $this->handleImageUpload($request);

        /**
         *  Create new object of Employee
         */
        $employee = new User();

        /**
         *  setEmployee is also a method of this controller
         *  which i have created, so i can use it for update
         *  method.
         */
        $this->setEmployee($employee,$request,$fileNameToStore);

        return redirect('/employees')->with('info','El empleado ha sido creado!');
    }





    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


        /**
         *  this is same as create but with an existing
         *  employee
         */
      //  $employee =      User::find($id);
        $oficina  =       Oficina::orderBy('nombre','asc')->get();
        $departamento =   Departamento::orderBy('nombre','asc')->get();
        $puesto       =   Puesto::orderBy('nombre','asc')->get();
        $rol      =       Role::get();
        $jefes     =      Role::with('users')->where('name', '=', 'boss')->get();
        $nivel =    Nivel::with('puestos')->get();



                            //$u = Historial::where('user_id', '=', $id)->with('jefe')->get();

        $u = User::find($id);

        $is_custom = $u->is_custom_vacation;


        $employees = User::with('oficinas')->with('departamentos')->with('puestos')->with('roles')->with('ujefes')->with('uLeaves')->find($id);

        $nivel_id = '';

        foreach($employees->puestos as $actual){

        $nivel_id = $actual->nivel_id;
        $puesto_id = $actual->id;

        }

        $puesto_nivel_actual  =    Puesto::find($nivel_id);

        if(!empty($puesto_id)){

            $puesto_actual  =    Puesto::where('id', '=', $puesto_id)->get();

        }else{

            $puesto_actual = '';
        }

        $zalary = User::with('zalario')->find($id);


        foreach($zalary->zalario as $zalaryo){

            $salario_actual = $zalaryo->salario;

        }

        if(empty($salario_actual)){
            $salario_actual = '';
        }

       /* $usadas =  Leave_user::with(['puesto' => function ($q) use ($user_dias_vacacion) {

            $q->select('*')->where('id', '=', $user_dias_vacacion); }])
            ->where('user_id', '=', $user_id)
            ->where('status', '=', 3)
            ->whereBetween('created_at', [$desde, $hasta])
            ->get()->sum('requested_days'); */







        $employees->dias_vacacion; /*Id tipo de vacaciones */

        $seleccionada = Leave::find($employees->dias_vacacion);


        $salidas = Leave::get(); /*populate dropdown*/


    /*  if($employees->dias_vacacion != null){
            $nivel_salida_seleccionado =   Leave::with('users')->find($id);
            $custom_vacation =  true; /*this validate checkbox if its custom vacations */
         //   $salidas = Leave::get(); /*populate dropdown*/

     /*   }else{

            $nivel_salida_seleccionado =   Leave::with('users')->find($id);
            $custom_vacation =  false;
            $salidas = Leave::get();

        } */


        return view('employees.edit')->with([
            'oficinas'         => $oficina,
            'departamentos'    => $departamento,
            'puestos'          => $puesto,
            'roles'            => $rol,
            'employee'         => $employees,
            'jefes'            => $jefes,
            'is_custom'         => $is_custom,
            'nivel_salida_seleccionado'  => $seleccionada,
            'dropdown_salidas' => $salidas,
            'niveles' => $nivel,
            'puesto_nivel' => $puesto_nivel_actual,
            'puesto_actual' => $puesto_actual,
            'salario_actual' => $salario_actual

        ]);
    }


     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {



        $request->user()->authorizeRoles(['admin']);


        $this->validateRequest($request,$id);


        $employee = User::find($id);
        $old_picture = $employee->picture;


        if($request->hasFile('picture')){
            //Upload the image
            $fileNameToStore = $this->handleImageUpload($request);
            //Delete the previous image
            Storage::delete('public/employee_images/'.$employee->picture);
        }else{
            $fileNameToStore = '';
        }

        /**
         *  updating an existing employee with setEmployee
         *  method
         */
        $this->setEmployee($employee,$request,$fileNameToStore);
        return redirect('/employees')->with('info','Se ha actualizado el empleado con exito!');
    }


    public function show($id)
    {
      //  $employees = User::find($id);
        $employees = User::with('oficinas')->with('departamentos')->with('puestos')->with('roles')->with('ujefes')->with('fechas')->find($id);


        $historico = Historial::with('usuario')->where('user_id', '=', $id)->with('departamento')->with('puesto')->with('jefe')->get();


        /*$Nivel =  DB::table('historials')
                        ->leftjoin('puestos', 'historials.puesto_id', '=', 'puestos.id')
                        ->leftjoin('niveles', 'puestos.nivel_id', '=', 'niveles.id')
                        ->select('historials.*','puestos.*', 'niveles.*' )
                        ->where('user_id', '=', 1)->get(); */

        $nivel = DB::select("Select
                                h.id,
                                h.salario as sueldo,
                                h.reasignado,
                                h.baja_temp,
                                j.name as jefe_nombre,
                                j.last_name as jefe_apellido,
                                d.nombre as depa_nombre,
                                o.nombre as oficina_nombre,
                                p.nombre as puesto_nombre,
                                n.nombre as nivel_nombre

                            from historials as h

                                inner  join users as j on h.jefe_id = j.id
                                inner  join oficinas as o on h.oficina_id = o.id
                                inner  join departamentos as d on h.departamento_id = d.id
                                inner  join puestos as p on h.puesto_id=p.id
                                inner  join niveles as n on p.nivel_id=n.id
                                where h.user_id='$id' ORDER BY h.id DESC; ");



/*
            $nuevo = [];

            foreach($employees->oficinas as $key => $value){

                    $nuevo[$key]['oficina'] = $value->nombre;

            }

            foreach($employees->departamentos as $key => $value){

                $nuevo[$key]['departamento'] = $value->nombre;

            }


            foreach($employees->puestos as $key => $value){

                $nuevo[$key]['puesto'] = $value->nombre;

/*
                if(empty($value->nivel_id)){

                    $nivel_id = 'sin asignar';

                }else{

                    $nivel_id = $value->nivel_id;

                }


                $haber =   Nivel::where('id', '=', $nivel_id)->orderBy('id', 'desc')->get();

               foreach($haber as $h){

                if(empty($h->nombre)){

                    $nuevo[$key]['nivel'] = 'no asignado';

                }else{

                    $nuevo[$key]['nivel'] = $h->nombre;
                }

               }

            }


            foreach($employees->fechas as $key => $value){

                $nuevo[$key]['salario'] = $value->salario;

            }

            foreach($employees->ujefes as $key => $value){

                $nuevo[$key]['jefe'] = $value->name.' '.$value->last_name;

            }

            foreach($employees->fechas as $key => $value){
                $nuevo[$key]['reasignado'] = $value->reasignado;
                $nuevo[$key]['baja_temp'] = $value->baja_temp;
            } */



            $profile = User::with('profileoficinas')->with('zalario')->with('profiledepartamentos')->with('profilepuestos')->with('roles')->find($id);

            return view('employees.show')->with([
            'historico' => $historico,
            'nivel' => $nivel,
            'profile' => $profile
        ]);
    }



    /**
     * Save a new resource or update an existing resource.
     *
     * @param  App\User $user
     * @param  \Illuminate\Http\Request  $request
     * @param  string $fileNameToStore
     * @return Boolean
     */
    private function setEmployee(User $user, Request $request,$fileNameToStore){




        $user->name         = $request->input('name');
        $user->last_name    = $request->input('last_name');
        $user->email        = $request->input('email');
        $user->age          = $request->input('age');
        $user->address      = $request->input('address');
        $user->gender       = $request->input('gender');
        $user->phone        = $request->input('phone');

        if($request->filled('password'))
        {
            $user->password = bcrypt($request->input('password'));
            $user->update(['email'=>$request->email, 'password'=>bcrypt($request->password)]);
        }else{

        }


        //Format Date then insert it to the database
        $user->join_date    = date('Y-m-d', strtotime(str_replace('-', '/', $request->input('join_date'))));
        //Format Date then insert it to the database
        $user->birth_date   = date('Y-m-d', strtotime(str_replace('-', '/', $request->input('birth_date'))));

        if($request->hasFile('picture')){
            $user->picture = $fileNameToStore;
        }

        $user->rfc                  = $request->input('rfc');
        $user->curp                 = $request->input('curp');
        $user->estado_civil         = $request->input('estado_civil');
        $user->tipo_contrato        = $request->input('tipo_contrato');
//        $user->salario               = $request->input('salario');




        if($request->input('vacaciones')){

            $new_leave = new Leave;
            $new_leave->nombre = 'Personalizada';
            $new_leave->dias = $request->input('vacaciones');
            $new_leave->save();

            $id_leave = $new_leave->id;

            $user->dias_vacacion = $id_leave;
            $user->is_custom_vacation = true;

            $is_custom = $user->is_custom_vacation;

          }else if($request->input('nivel_salidas')){ /*Vacaciones Predeterminadas */

            if(isset($id_leave)){

                $salida = Leave::find($id_leave);
                $user->dias_vacacion = $salida->id;
                $is_custom =  $user->is_custom_vacation;

            }else{

                $user->dias_vacacion = $request->input('nivel_salidas');
                $user->is_custom_vacation = false;
               $is_custom =  $user->is_custom_vacation;
            }



          }

       $user->save();

        //$last_inserted_id =  User::find($user->id);


        $user->roles()->sync($request->input('role'));

       // dd($last_inserted_id->id);
        /**
         * This part is to save to a Historic Positions of the Company
         */



        $historial = new Historial;
        $historial->user_id =      $user->id;
        $historial->oficina_id   = $request->input('oficina');
        $historial->departamento_id    = $request->input('departamento');
        $historial->puesto_id    = $request->input('puesto');
        $historial->salario = $request->input('salario');




        $historial->jefe_id    = $request->input('jefe');

        if($d_date=strtotime(str_replace('-', '/', $request->input('reasignado')))) {
            // Since we have a valid time, turn to date string
            $historial->reasignado=date("Y-m-d", $d_date);

          }

          if($baja_date=strtotime(str_replace('-', '/', $request->input('baja_temp')))) {
            // Since we have a valid time, turn to date string
            $historial->baja_temp=date("Y-m-d", $baja_date);

          }



       // $historial->reasignado = date('Y-m-d', strtotime(str_replace('-', '/', $request->input('reasignado'))));
       // $historial->baja_temp =  date('Y-m-d', strtotime(str_replace('-', '/', $request->input('baja_temp'))));


        $historial->save();



        $perfil_actual = Profile::firstOrNew(['user_id' => $user->id]);
        $perfil_actual->user_id =  $user->id;
        $perfil_actual->oficina_id   = $request->input('oficina');
        $perfil_actual->departamento_id    = $request->input('departamento');
        $perfil_actual->puesto_id    = $request->input('puesto');
        $perfil_actual->jefe_id    = $request->input('jefe');
        $perfil_actual->salario = $request->input('salario');
        $perfil_actual->reasignado = date('Y-m-d', strtotime(str_replace('-', '/', $request->input('reasignado'))));
        $perfil_actual->baja_temp =  date('Y-m-d', strtotime(str_replace('-', '/', $request->input('baja_temp'))));


        $perfil_actual->save();

        return $perfil_actual;

    //$user->uHistorial()->detach(['user_id' => $historial->user_id, 'oficina_id'=> $historial->oficina_id, 'departamento_id'=> $historial->$departamento_id, 'puesto_id'=> $historial->puesto_id,  ]);
    //$user->uHistorial()->detach(['user_id' => $historial->user_id, 'oficina_id'=> $historial->oficina_id, 'departamento_id'=> $historial->$departamento_id, 'puesto_id'=> $historial->puesto_id,  ]);




    }




 /**
     * This method is used for validating the form
     *
     * @param  \Illuminate\Http\Request  $request
     * @return $this
     */
    private function validateRequest_crear($request,$id){




        /**
         *  specifying the validation rules
         */
        /**
         *  Below in Picture validation rules we are first checking
         *  that if there is an image, if not then don't apply the
         *  validation rules. the reason we are doing this is because
         *  if we are updating an employee but not updating the image.
         */
        return $this->validate($request,[
            'name'     =>  'required|min:3|max:50',
            'last_name'      =>  'required|min:3|max:50',
            'email'          =>  'required|email|unique:users,email,'.($id ? : '' ).'|max:250',
            'password'       =>  'confirmed|required|min:6',
            'password_confirmation'       =>  ':confirmed|required|min:6',
            'phone'          =>  'required|max:13',
            'address'        =>  'required|min:10|max:500',
            'join_date'      =>  'required',
            'birth_date'     =>  'required',
            'age'            =>  'required|min:2|max:2',
            'gender'         => 'required',
            'rfc'            => 'required',
            'curp'           => 'required',
            'tipo_contrato'  => 'required',
            'estado_civil'   => 'required',
            'age'            => 'required',
            'oficina'        => 'required',
            'departamento'   => 'required',
            'role'           => 'required',
            'salario'        => 'required',
            'picture'        =>  ($request->hasFile('picture') ? 'required|image|max:1999' : ''),



            /**
             *  if we are updating an employee but not changing the
             *  email then this will throw a validation error saying
             *  that email should be unique. that's why we need to specify
             *  the current employee to ignore the unique validation rule.
             *  Above in email rules , we are using a ternary operator simply
             *  saying that if we pass an id then it will ignore that employee
             *  (which we want in update) and if id's null then it will check
             *  every employee to be unique (which we want in create because
             *  every employee should have a unique email).
             *  check the documentation for more details,
             *  https://laravel.com/docs/5.6/validation#rule-unique
             */


        ]);
    }



 /**
     * This method is used for validating the form
     *
     * @param  \Illuminate\Http\Request  $request
     * @return $this
     */
    private function validateRequest($request,$id){




        /**
         *  specifying the validation rules
         */
        /**
         *  Below in Picture validation rules we are first checking
         *  that if there is an image, if not then don't apply the
         *  validation rules. the reason we are doing this is because
         *  if we are updating an employee but not updating the image.
         */
        return $this->validate($request,[
            'name'     =>  'required|min:3|max:50',
            'last_name'      =>  'required|min:3|max:50',
            'email'          =>  'required|email|unique:users,email,'.($id ? : '' ).'|max:250',
            'password'       =>  'sometimes|confirmed',
            'password_confirmation'       =>  'sometimes|required_with:password|same:password',
            'phone'          =>  'required|max:13',
            'address'        =>  'required|min:10|max:500',
            'join_date'      =>  'required',
            'birth_date'     =>  'required',
            'age'            =>  'required|min:2|max:2',
            'gender'        =>    'required',
            'picture'        =>  ($request->hasFile('picture') ? 'required|image|max:1999' : '')

            /**
             *  if we are updating an employee but not changing the
             *  email then this will throw a validation error saying
             *  that email should be unique. that's why we need to specify
             *  the current employee to ignore the unique validation rule.
             *  Above in email rules , we are using a ternary operator simply
             *  saying that if we pass an id then it will ignore that employee
             *  (which we want in update) and if id's null then it will check
             *  every employee to be unique (which we want in create because
             *  every employee should have a unique email).
             *  check the documentation for more details,
             *  https://laravel.com/docs/5.6/validation#rule-unique
             */


        ]);
    }



      /**
     * Handle image upload when creating a new resource
     * or updating an existing resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    public function handleImageUpload(Request $request){
        if( $request->hasFile('picture') ){

            //get filename with extension
            $filenameWithExt = $request->file('picture')->getClientOriginalName();
            //get just filename
            $filename = pathInfo($filenameWithExt,PATHINFO_FILENAME);
            // get just extension
            $extension = $request->file('picture')->getClientOriginalExtension();

            /**
             * filename to store
             *
             *  we are appending timestamp to the file name
             *  and prepending it to the file extension just to
             *  make the file name unique.
             */
            $fileNameToStore = $filename.'_'.time().'.'.$extension;

            //upload the image
            $path = $request->file('picture')->storeAs('public/employee_images',$fileNameToStore);
        }
        /**
         *  return the file name so we can add it to database.
         */

        return  $fileNameToStore;
    }


     /**
     *  Search For Resource(s)
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request){
        $this->validate($request,[
            'search'   => 'required|min:1',
            'options'  => 'required'
        ]);
        $str = $request->input('search');
        $option = $request->input('options');
        $employees = User::where($option, 'LIKE' , '%'.$str.'%')->Paginate(4);
        return view('employee.index')->with(['employees' => $employees , 'search' => true ]);
    }





    public function departamento($id)
{


    $depas = DB::table('departamentos')->orderBy('name', 'asc')->get();

    dd($depas);

//    echo json_encode($depas);
//    exit;
}








}
