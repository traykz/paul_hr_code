<?php

namespace App\Http\Controllers;


use App\Role;
use App\User;
use App\Oficina;
use App\Departamento;
use App\Puesto;
use App\Leaves;
use App\Historial;

use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;



class DepartamentosController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }



      /**
     *  Search For Resource(s)
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
        public function search(Request $request){
            $this->validate($request,[
                'search'   => 'required|min:1',
                'options'  => 'required'
            ]);
            $str = $request->input('search');
            $option = $request->input('options');
            $departamentos = Departamento::where($option, 'LIKE' , '%'.$str.'%')->Paginate(4);
            return view('departamentos.index')->with(['departamentos' => $departamentos , 'search' => true ]);
        }


  /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
        public function index(Request $request)
        {
            $request->user()->authorizeRoles(['admin']);

            $departamentos = Departamento::get();
            return view('departamentos.index')->with('departamentos',$departamentos);
        }



     /**
     * Edita a listing of the resource.
     * @param int
     * @return \Illuminate\Http\Response
     */

        public function edit(Request $resquest, $id){
                $departamento = Departamento::find($id);
                return view('departamentos.edit')->with('departamento' ,$departamento);
        }


             /**
     * Edit a existing resource
     *
     *  @param \Illuminate\Http\Request
     *  @param  int  $id
     *  @return \Illuminate\Http\Response
     */

     public function update(Request $request, $id){
        $request->user()->authorizeRoles(['admin']);

        $this->validateRequest($request,$id);

        $departamento = Departamento::find($id);

        $departamento->nombre = $request->input('nombre');
        $departamento ->save();

        return redirect('/departamentos')->with('info','Se ha actualizado el departamento con exito!');

     }



/**
     * This method is used for validating the form
     *
     * @param  \Illuminate\Http\Request  $request
     * @return $this
     */
    private function validateRequest($request,$id){

        return $this->validate($request,[
            'nombre'     =>  'required|min:3|max:50',
            ]);

        }






    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
         public function create()
        {

            return view('departamentos.create');
        }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->user()->authorizeRoles(['admin']);

        $this->validate($request,[
            'nombre' => 'required|min:4|unique:departamentos',
        ]);

        $departamento = new Departamento();
        $departamento->nombre = $request->input('nombre');
        $departamento->save();

         return redirect('/departamentos')->with('info','Departamento Creado!');
    }


}
