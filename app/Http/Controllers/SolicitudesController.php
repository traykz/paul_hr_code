<?php

namespace App\Http\Controllers;

use App\Leave_user;
use App\Leave;
use App\User;
use App\Profile;
use Auth;
use Carbon\Carbon;
use Code16\CarbonBusiness\BusinessDays;
use Mail;
use DB;
use Response;

use Illuminate\Notifications\Notification;
use App\Notifications\SolicitudAprobada;
use App\Notifications\SolicitudCancelada;

use Illuminate\Notifications\Notifiable;

use Illuminate\Support\Collection;

use Illuminate\Http\Request;

use PDF;

use Storage;

class SolicitudesController extends Controller
{

    use Notifiable;

    public function __construct(){
        $this->middleware('auth');
    }


    public function index(Request $request){
    $permisos = ['admin', 'boss'];
    $request->user()->authorizeRoles($permisos);
    $user_id = $request->user()->id;
    $tipo = $request->user()->que_role($user_id);


    if($tipo === 'boss'){
    $solicitudes_jefe = Profile::with('user')->with('jefe')->where('jefe_id', '=', $user_id)->get();
    $colleccion = new Collection;
    foreach($solicitudes_jefe as $solicita){
       // $jefe_id =  $solicita->jefe->id;
        $solicita_id =  $solicita->user->id;
        $add_solicitud = Leave_user::with('user')->with('leavez')->where('user_id', '=', $solicita_id)->get();
    if($add_solicitud->count() > 0)
        {
            $colleccion->push($add_solicitud);
        }
    }

    return view('leaves.solicitudes.index')->with([
        'solicitudes_jefe' => $colleccion,
        'tipo' => $tipo
      ]);

    }elseif ( $tipo === 'admin'){

      // $disponibles =  $this->howMany();

       $todas = Leave_user::with('user')->with('leavez')->get();



       $date_current = date('Y-m-d H:i:s');
       $prev_date1 = date('2019-08-01');
       $prev_date2 = date('2019-09-01');
       $prev_date3 = date('2019-11-01');
       $prev_date4 = date('2019-12-20');


       $emp_count_1 = User::whereBetween('join_date',[$prev_date1,$date_current])->count();
       $emp_count_2 = User::whereBetween('join_date',[$prev_date2,$prev_date1])->count();
       $emp_count_3 = User::whereBetween('join_date',[$prev_date3,$prev_date2])->count();
       $emp_count_4 = User::whereBetween('join_date',[$prev_date4,$prev_date3])->count();


       $t_employees = User::all()->count();
       $t_admins = 0;
       $t_countries = 0;
       $t_states = 0;
       $t_cities = 0;
       $t_departments = 0;
       $t_divisions = 0;
       $t_salaries = 0;
       $t_leaves = 1;


        return view('leaves.solicitudes.index')->with([


            'emp_count_1'     =>  $emp_count_1,
            'emp_count_2'     =>  $emp_count_2,
            'emp_count_3'     =>  $emp_count_3,
            'emp_count_4'     =>  $emp_count_4,
            't_employees'     =>  $t_employees,
            't_countries'     =>  $t_countries,
            't_cities'        =>  $t_cities,
            't_states'        =>  $t_states,
            't_salaries'      =>  $t_salaries,
            't_divisions'     =>  $t_divisions,
            't_departments'   =>  $t_departments,
            't_admins'        =>  $t_admins,
            't_leaves'        =>  $t_leaves,
            'todas' => $todas,
            'tipo' => $tipo
          ]);
    } else {
        return view('leaves.solicitudes.index');
    }
}


    public function getSalidas(Request $request, $id){



    $registro = Leave_user::with('user')->where('user_id', '=', $id)->get();

    foreach($registro as $reg){

        $user_dias_vacacion = $reg->user->dias_vacacion;
        $is_customized = $reg->user->is_custom_vacation;
        $user_id = $reg->user->id;

    }


    $año_actual = Carbon::now()->year;
    $mes_inicio = 1;
    $dia_inicio = 1; //1ro de Enero
    $mes_final = 12;
    $dia_final = 31; //31 de diciembre

    $desde  =  Carbon::createFromDate($año_actual, $mes_inicio, $dia_inicio);
    $hasta  =  Carbon::createFromDate($año_actual, $mes_final, $dia_final);


    $usadas =  Leave_user::with(['leavez' => function ($q) use ($user_dias_vacacion) {

        $q->select('*')->where('id', '=', $user_dias_vacacion); }])
        ->where('user_id', '=', $user_id)
        ->where('status', '=', 3)
        ->whereBetween('created_at', [$desde, $hasta])
        ->get()->sum('requested_days');




    $asignadas = Leave::where('id', '=', $user_dias_vacacion)->get();


    foreach($asignadas as $avaiable){
       $tipo_id = $avaiable->id;
       $tipo = $avaiable->nombre;
       $total = $avaiable->dias;
    }

    $final = $total - $usadas;

    if($final < 0  ){
        $final = 0;
    }

    $data['asignadas'] = $total;
    $data['usadas'] = $usadas;
    $data['disponibles'] = $final;


    return Response::json($data);



}
    public function create(Request $request){



        $today = Carbon::now();
        $leaves = Leave::get();
        $user_id = $request->user()->id;

        $has_custom = User::find($user_id);

        if($has_custom != null){
            $custom_leave = Leave::find($has_custom->dias_vacacion);
        }else{

            $custom_leave =  null;

        }



        return view('leaves.solicitudes.create')->with([
            'today' => $today,
            'leaves' => $leaves,
            'has' => $has_custom,
            'custom_leave' => $custom_leave
            ]);


    }
    private function validateRequest($request,$id){
        return $this->validate($request,[
            'custome_leave' => 'sometimes',
            'leave_id'       =>  'sometimes',
            'request_from'   =>  'required',
            'request_at'     =>  'required',
            'comentarios'    =>  'required',
        ]);
    }
         /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validateRequest($request,null);
        $user = new User();
        $this->setEmployee($user,$request);

        return redirect('home')->with('info','La solicitud ha sido creada!');
    }



    /**
     * Save a new resource or update an existing resource.
     *
     * @param  App\User $user
     * @param  \Illuminate\Http\Request  $request
     * @param  string $fileNameToStore
     * @return Boolean
     */
    private function setEmployee(User $user,Request $request){


        //$user = User::find(Auth::user()->id)->with('roles')->with('uLeaves')->with('salidas')->get();
        $leave = new Leave_user;
        $leave->user_id =  Auth::user()->id;




         if($request->input('custom_leave') != null){

             $leave->leave_id  = (int)$request->input('custom_leave');

         }else{

              $leave->leave_id = (int)$request->input('leave_id');
         }



 $from   = date('Y-m-d', strtotime(str_replace('-', '/', $request->input('request_from'))));
 //Format Date then insert it to the database
 $to  = date('Y-m-d', strtotime(str_replace('-', '/', $request->input('request_at'))));


 $fecha_desde = Carbon::parse($from);

 echo $fecha_desde;

    $m_desde = $fecha_desde->month;
    $d_desde = $fecha_desde->day;
    $a_desde = $fecha_desde->year;



 $fecha_hasta = Carbon::parse($to);
echo $fecha_hasta;
 $m_hasta = $fecha_hasta->month;
 $d_hasta = $fecha_hasta->day;
 $a_hasta = $fecha_hasta->year;



 /** PENDIENTE CREAR DIAS FERIADOS Y FINES DE SEMANA */
 $date = new BusinessDays();
 $days = $date->daysBetween(
    Carbon::createFromDate($a_desde, $m_desde, $d_desde),
    Carbon::createFromDate($a_hasta, $m_hasta, $d_hasta)

 );


 //print $prueba->diffInDays($prueba2);



 // $leave->comentarios    = $request->input('comentarios');
 $leave->requested_from = $fecha_desde;
 $leave->requested_at = $fecha_hasta;
 $leave->requested_days = $days;
 $leave->status = 1;


 $leave->save();

        return $leave;

    }


    public function pdf($registro){

        $fecha = Carbon::now();


        dd($registro);

        return view('leaves.solicitudes.acuse')->with([

            'solicitudes' => $fecha,
            'registro' => $registro

          ]);

    }
    public function preaprobar(Request $request, $id){

        $method = $request->method();
        $registro = Leave_user::with('user')->get()->find($id);
        $solicitados = $registro->requested_days;
        $user_dias_vacacion = $registro->user->dias_vacacion;
        $user_id = $registro->user->id;

        $año_actual = Carbon::now()->year;
        $mes_inicio = 1;
        $dia_inicio = 1; //1ro de Enero
        $mes_final = 12;
        $dia_final = 31; //31 de diciembre
        $desde  =  Carbon::createFromDate($año_actual, $mes_inicio, $dia_inicio);
        $hasta  =  Carbon::createFromDate($año_actual, $mes_final, $dia_final);


        $usadas =  Leave_user::with(['leavez' => function ($q) use ($user_dias_vacacion) {

            $q->select('*')->where('id', '=', $user_dias_vacacion); }])
            ->where('user_id', '=', $user_id)
            ->where('status', '=', 3)
            ->whereBetween('created_at', [$desde, $hasta])
            ->get()->sum('requested_days');


        $disponibles = Leave::where('id', '=', $user_dias_vacacion)->get();


        foreach($disponibles as $avaiable){
           $tipo_id = $avaiable->id;
           $tipo = $avaiable->nombre;
           $total = $avaiable->dias;
        }



        $utilizados = $usadas; // Dias utilizados..
        if($total <= $utilizados){


            return redirect('/solicitudes')->with('info','Solicitud de Vacaciones Rechazada, Utilizados '. $utilizados.'favor de cancelarla.');



        }else{

            $cuantos = $total - $utilizados;

       //     echo 'solicita'. $solicitados .'<br>';
       //     echo 'tiene disponibles'. $cuantos;

            if($solicitados > $cuantos){
                  //  echo 'ALV, Solicitó '.$solicitados.' y solo le quedan '. $cuantos;

                    return redirect('/solicitudes')->with('info','Solicitud de Vacaciones Rechazada, Utilizados  '. $cuantos.' favor de cancelarla.');

                }else{
                    $operacion = $cuantos - $solicitados;

                        $registro->status = 2;
                        $registro->save();

                        $data = $registro->toArray();

                        $user_id = $registro->user->id;

                        $e_info = User::where('id', '=', $user_id)->with('profileoficinas')->with('profiledepartamentos')->with('profilepuestos')->with('roles')->get()->toArray();

                        $fecha = Carbon::now();
                        $usados = $utilizados;
                        $pendientes = $operacion;

                        $pdf = PDF::loadView('leaves.solicitudes.acuse', [
                            'solicitudes' => $fecha,
                            'registro' => $registro,
                            'usados' => $usados,
                            'pendientes' => $pendientes,
                            'disponibles' => $cuantos
                            ]);


                             $content = $pdf->download()->getOriginalContent();

                             Storage::put('public/pdf/'.$registro->id.$fecha->year.$fecha->month.$fecha->day.'.pdf', $content);

                            $registro->user->notify(new SolicitudAprobada($data, $e_info, $content));


                             return redirect('/solicitudes')->with('info','Solicitud de Vacaciones Aceptada!, Disponibles '. $pendientes);


                }
        }




/*

        $validar = Leave_user::select([
            DB::raw('DATE(created_at) AS date'),
            DB::raw('COUNT(id) AS count'),
            DB::raw('SUM(requested_days) as dias_usados')
         ])
         ->where('user_id', '=', $registro->user->id)
         ->WHERE('status', '=', 2)
         ->where('leave_id', '=', $tipo_id)
         ->whereBetween('created_at', [$desde, $hasta])
         ->groupBy('date')
         ->orderBy('date', 'ASC')
         ->get()
         ->toArray();

         if($validar){

            foreach ( $validar as $v){
                if($v['dias_usados'] <= $total){
                    $quedan = $total-$v['dias_usados'];
                    $mensaje_ok = $quedan;
                    if($quedan >= $registro->requested_days){

                        $registro->status = 2;
                        $registro->save();


                       $data = $registro->toArray();
                       $user_id = $registro->user->id;
                       $e_info = User::where('id', '=', $user_id)->with('profileoficinas')->with('profiledepartamentos')->with('profilepuestos')->with('roles')->get()->toArray();

                       $fecha = Carbon::now();
                       $usados = $v['dias_usados'];
                       $pendientes = $quedan;

                       $pdf = PDF::loadView('leaves.solicitudes.acuse', [
                           'solicitudes' => $fecha,
                           'registro' => $registro,
                           'usados' => $usados,
                           'pendientes' => $pendientes
                           ]);


                            $content = $pdf->download()->getOriginalContent();

                            Storage::put('public/pdf/'.$registro->id.$fecha->year.$fecha->month.$fecha->day.'.pdf', $content);

                           $registro->user->notify(new SolicitudAprobada($data, $e_info, $content));


                        return redirect('/solicitudes')->with('info','Solicitud de Vacaciones Aceptada!, Disponibles'. $mensaje_ok);

                    } else{
                        $mensaje_error = 'La cantidad de dias solicitados es superior a los Disponibles (Tiene  '.$quedan .' dias para el tipo: '.$tipo.')';
                        return redirect('/solicitudes')->with('info', $mensaje_error);
                    }
                }else{
                    $mensaje_error = 'No le quedan dias por utilizar';
                    return redirect('/solicitudes')->with('info', $mensaje_error);
                 }

               }

         }else{
                 $v['dias_usados'] = 0;
                if($v['dias_usados'] <= $total){
                    $quedan = $total-$v['dias_usados'];
                    $mensaje_ok = $quedan.' disponibles';
                    if($quedan >= $registro->requested_days){
                        $registro->status = 2;
                        $registro->save();
                        $data = $registro->toArray();
                        $user_id = $registro->user->id;
                        $e_info = User::where('id', '=', $user_id)->with('profileoficinas')->with('profiledepartamentos')->with('profilepuestos')->with('roles')->get()->toArray();
                       // $this->sendMail($mensaje);
                        $fecha = Carbon::now();
                        $usados = $v['dias_usados'];
                        $pendientes = $quedan;

                       $pdf = PDF::loadView('leaves.solicitudes.acuse', [
                        'solicitudes' => $fecha,
                        'registro' => $registro,
                        'usados' => $usados,
                        'pendientes' => $pendientes
                        ]);


                         $content = $pdf->download()->getOriginalContent();

                         Storage::put('public/pdf/'.$registro->id.$fecha->year.$fecha->month.$fecha->day.'.pdf', $content);


                        $registro->user->notify(new SolicitudAprobada($data, $e_info, $content));

                        return redirect('/solicitudes')->with('info','Solicitud de Vacaciones Aceptada!,  tenia'. $mensaje_ok);


                    } else{
                        $mensaje_error = 'La cantidad de dias solicitados es superior a los Disponibles ('.$quedan .' para .'.$tipo.')';
                        return redirect('/solicitudes')->with('info', $mensaje_error);
                    }

                }else{
                    $mensaje_error = 'No le quedan dias por utilizar';
                    return redirect('/solicitudes')->with('info', $mensaje_error);
                 }


            }


       // $nombre = $registro->user->name.' '.$registro->user->last_name;
       // $email = $registro->user->email;

*/
    }
    public function aceptar(Request $request, $id){

        $method = $request->method();

        $registro = Leave_user::with('user')->get()->find($id);

                        $registro->status = 3;
                        $registro->save();


                        //$registro->user->notify(new SolicitudAprobada($data, $e_info, $content));


                        return redirect('/solicitudes')->with('info','Solicitud  Aceptada, Favor de Recibir su Acuse Firmado!');
    }


    public function acept_depreciada(Request $request, $id)
    {

        $hoy = Carbon::now();
        $year_now = $hoy->year;
        $method = $request->method();
        $registro = Leave_user::with('user')->with('leavez')->get()->find($id);
        $solicitud_id = $registro->id;
        $user_id = $registro->user_id;
        $days_solicitados = $registro->requested_days;
        $query = Disponible::where('status', '=', 1)->get();
        $dias_disponibles = ($query->sum('disponibles'));
        $cuantos_son = count($query);
        echo 'dias solicitados: '.$days_solicitados.'<br>';
        echo 'dias disponibles '.$dias_disponibles.'<br>';

        if ($days_solicitados <= $dias_disponibles) {
            $query2 = Disponible::where('status', '=', 1)->orderBy('created_at', 'ASC')->limit(1,0)->get();
            foreach($query2 as $query){
                $id_registro = $query->id;
                $disponibles =   $query->disponibles;
            }
            if($days_solicitados <= $disponibles){ /*lo puede descontar de un solo registro*/
                echo 'con uno basta';
                $final =  $disponibles - $days_solicitados;
                $find_reg = Disponible::find($id_registro);
                $find_reg->disponibles = $final;
                $find_reg->save();
                $resultado = $find_reg->disponibles;
                if($resultado === 0){
                    $find_reg->status = 0;
                    $find_reg->save();
                }
                /*aqui acaba nuestra regla*/
            }elseif($days_solicitados > $disponibles){ /*necesita mas de dos registros*/
                echo 'necesita descontar en '.$cuantos_son.' registros';
                $final2=  $days_solicitados - $disponibles;
                $find_reg = Disponible::find($id_registro);
                $find_reg->disponibles = 0;
                $find_reg->save();
                $resultado = $find_reg->disponibles;
                if($resultado === 0){
                    $find_reg->status = 0;
                    $find_reg->save();
                    if($final2 !== 0){
                        $query2 = Disponible::where('status', '=', 1)->orderBy('created_at', 'ASC')->limit(1,0)->get();
                        foreach($query2 as $query){
                            $id_registro = $query->id;
                            $disponibles =   $query->disponibles;
                        }
                        if($final2 <= $disponibles){ /*lo puede descontar de un solo registro*/
                            echo 'con uno basta';
                            $final =  $disponibles - $final2;
                            $find_reg = Disponible::find($id_registro);
                            $find_reg->disponibles = $final;
                            $find_reg->save();
                            $resultado = $find_reg->disponibles;
                            echo 'sobran '.$resultado;
                            if($resultado === 0){
                                $find_reg->status = 0;
                                $find_reg->save();
                            }elseif($resultado > 1){
                                $find_reg->disponibles = $resultado;
                                $find_reg->save();
                            }
                            /*aqui acaba nuestra regla*/
                        }elseif($final2 > $disponibles){ /*necesita mas de dos registros*/
                            echo 'segundo necesita descontar en  '.$cuantos_son.' registros';
                            $final3 =  $final2 - $disponibles;
                            $find_reg = Disponible::find($id_registro);
                            $find_reg->disponibles = 0;
                            $find_reg->save();
                            $resultado = $find_reg->disponibles;
                            if($resultado === 0){
                                $find_reg->status = 0;
                                $find_reg->save();
                                if($final3 !== 0){
                                    $query2 = Disponible::where('status', '=', 1)->orderBy('created_at', 'ASC')->limit(1,0)->get();
                                    foreach($query2 as $query){
                                        $id_registro = $query->id;
                                        $disponibles =   $query->disponibles;
                                    }
                                    if($final3 <= $disponibles){ /*lo puede descontar de un solo registro*/
                                        echo 'con uno basta';
                                        $final =  $disponibles - $final3;
                                        $find_reg = Disponible::find($id_registro);
                                        $find_reg->disponibles = $final;
                                        $find_reg->save();
                                        $resultado = $find_reg->disponibles;
                                        echo 'sobran '.$resultado;
                                        if($resultado === 0){
                                            $find_reg->status = 0;
                                            $find_reg->save();
                                        }elseif($resultado > 1){
                                            $find_reg->disponibles = $resultado;
                                            $find_reg->save();
                                        }
                                        /*aqui acaba nuestra regla*/
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }elseif($days_solicitados > $dias_disponibles){
            echo 'simplemente no le alcanzan los dias';
        }
    }



    public function rechazar(Request $request, $id){
        $registro = Leave_user::with('user')->get()->find($id);
        $registro->status = 4;
        $registro->save();
        $data = $registro->toArray();
        $user_id = $registro->user->id;
        $e_info = User::where('id', '=', $user_id)->with('profileoficinas')->with('profiledepartamentos')->with('profilepuestos')->with('roles')->get()->toArray();
       // $this->sendMail($mensaje);
        $registro->user->notify(new SolicitudCancelada($data, $e_info));
        return redirect('/solicitudes')->with('info','Solicitud de Vacaciones Cancelada!');
    }


    public function sendMail($mensaje){

            $array = [

                'texto' => $mensaje

            ];

        Mail::send('emails.email', $array, function ($message) {

            $message->to('mgorostizaga@bluelabel.mx', 'Miguel Angel ')

                    ->subject('Recursos Humanos');
        });

        if (Mail::failures()) {
           return response()->json('Sorry! Intenta más tarde');
         }else{
           return response()->json('Email enviado correctamente');
         }


    }



    public function howMany(){


        $users = User::get();
        foreach($users as $u){


         /*   $ux =  User::with(['salidas' => function ($q) use ($u) {
                $q->select(DB::raw('sum(requested_days) as total'),'id','user_id','leave_id','status', 'requested_at', 'requested_from', 'requested_days')
                 ->where('user_id', $u->id);
                 }])->get(); */
            /* $leaves =  Leave_user::
                     select(DB::raw('sum(requested_days) as total'),'id','user_id','leave_id','status', 'requested_at', 'requested_from', 'requested_days')
                     ->where('user_id', $u->id)
                     ->get(); */

        $leaves =  Leave_user::select('id','user_id','leave_id','status', 'requested_at', 'requested_from', 'requested_days')
                 ->where('user_id', $u->id)->with('user')->with('leavez')
                 ->get();


                foreach($leaves as $key => $value){
                        $usuarios[$value->id] =  $value;
                };



        }


        return ($usuarios);





    }
    public function update(){
        echo 'update';
    }
    public function edit(){
        echo 'edit';
    }
    public function show(){
        echo 'show';
    }
    public function search(){

    }


}
