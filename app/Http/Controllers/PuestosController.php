<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use App\Oficina;
use App\Departamento;
use App\Puesto;
use App\Leaves;
use App\Historial;
use App\Nivel;

use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;



class PuestosController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

      /**
     *  Search For Resource(s)
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
        public function search(Request $request){
            $this->validate($request,[
                'search'   => 'required|min:1',
                'options'  => 'required'
            ]);
            $str = $request->input('search');
            $option = $request->input('options');
            $puestos = Puesto::where($option, 'LIKE' , '%'.$str.'%')->Paginate(4);
            return view('puestos.index')->with(['puestos' => $puestos , 'search' => true ]);
        }


  /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
        public function index(Request $request)
        {
            $request->user()->authorizeRoles(['admin']);

            $puestos = Puesto::get();
            return view('puestos.index')->with('puestos',$puestos);
        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
         public function create()
        {
            $nivel = Nivel::get();
            return view('puestos.create')->with([

                'niveles' => $nivel

                ]);        }




    public function edit($id){

        $puesto = Puesto::find($id);

        $nivel = Nivel::get();

        return view('puestos.edit')->with([

            'oficina' => $puesto,
            'niveles' => $nivel

            ]);

     }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->user()->authorizeRoles(['admin']);

        $this->validate($request,[
            'nombre' => 'required|min:4|unique:puestos',
            'nivel' => 'required',
        ]);

        $puesto = new Puesto();
        $puesto->nombre = $request->input('nombre');
        $puesto->salario = 0;
        $puesto->nivel_id = $request->input('nivel');

        $puesto->save();

         return redirect('/puestos')->with('info','Puesto Creado!');
    }




             /**
     * Edit a existing resource
     *
     *  @param \Illuminate\Http\Request
     *  @param  int  $id
     *  @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id){



        $request->user()->authorizeRoles(['admin']);

        $this->validateRequest($request,$id);

        $puesto = Puesto::find($id);

        $puesto->nombre = $request->input('nombre');
        $puesto->salario = 0;
        $puesto->nivel_id = $request->input('nivel');

        $puesto ->save();

        return redirect('/puestos')->with('info','Se ha actualizado el departamento con exito!');

     }





/**
     * This method is used for validating the form
     *
     * @param  \Illuminate\Http\Request  $request
     * @return $this
     */
    private function validateRequest($request,$id){

        return $this->validate($request,[
            'nombre'     =>  'required|min:3|max:50',
            'nivel'      =>  'required'
            ]);

        }







}

