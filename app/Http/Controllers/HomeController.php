<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Leave_user;
use App\Leave;
use Carbon\Carbon;
use DB;
use Response;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }



    private function getPrevDate($num){
        return Carbon::now()->subMonths($num)->toDateTimeString();
    }



    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
   /* public function index(Request $request)
    {
        $request->user()->authorizeRoles(['user', 'admin', 'boss']);
        return view('home');
    } */



    public function index(Request $request){


        $date_current = Carbon::now()->toDateTimeString();
        $permisos = ['admin', 'boss', 'user'];

        $request->user()->authorizeRoles($permisos);

        $id = $request->user()->id;

        $tipo = $request->user()->que_role($id);


        $cancelado = Leave_user::where([
            ['status', '=', 'cancelado'],
            ['user_id' , '=', $id]
        ])->with('leavez')->get();



        $registro = Leave_user::with('user')->where('user_id', '=', $id)->get();



        $registro2 = User::with('uLeaves')->where('id', '=', $id)->get();




            if(count($registro)){
                foreach($registro as $reg){

                    $user_dias_vacacion = $reg->user->dias_vacacion;
                    $is_customized = $reg->user->is_custom_vacation;
                    $user_id = $reg->user->id;

                }



            $año_actual = Carbon::now()->year;
            $mes_inicio = 1;
            $dia_inicio = 1; //1ro de Enero
            $mes_final = 12;
            $dia_final = 31; //31 de diciembre

            $desde  =  Carbon::createFromDate($año_actual, $mes_inicio, $dia_inicio);
            $hasta  =  Carbon::createFromDate($año_actual, $mes_final, $dia_final);


            $usadas =  Leave_user::with(['leavez' => function ($q) use ($user_dias_vacacion) {

                $q->select('*')->where('id', '=', $user_dias_vacacion); }])
                ->where('user_id', '=', $user_id)
                ->where('status', '=', 3)
                ->whereBetween('created_at', [$desde, $hasta])
                ->get()->sum('requested_days');




            $asignadas = Leave::where('id', '=', $user_dias_vacacion)->get();


            foreach($asignadas as $avaiable){
            $tipo_id = $avaiable->id;
            $tipo = $avaiable->nombre;
            $total = $avaiable->dias;
            }
            $final = $total - $usadas;
            if($final < 0  ){
                $final = 0;
            }
            }elseif(count($registro2)){
            foreach($registro2 as $reg){
               $user_dias_vacacion = $reg->dias_vacacion;
               $is_customized = $reg->dias_vacacion;
               $user_id = $reg->id;
            }
            $año_actual = Carbon::now()->year;
            $mes_inicio = 1;
            $dia_inicio = 1; //1ro de Enero
            $mes_final = 12;
            $dia_final = 31; //31 de diciembre
            $desde  =  Carbon::createFromDate($año_actual, $mes_inicio, $dia_inicio);
            $hasta  =  Carbon::createFromDate($año_actual, $mes_final, $dia_final);
            $usadas =  Leave_user::with(['leavez' => function ($q) use ($user_dias_vacacion) {
                $q->select('*')->where('id', '=', $user_dias_vacacion); }])
                ->where('user_id', '=', $user_id)
                ->where('status', '=', 3)
                ->whereBetween('created_at', [$desde, $hasta])
                ->get()->sum('requested_days');
            $asignadas = Leave::where('id', '=', $user_dias_vacacion)->get();
            foreach($asignadas as $avaiable){
            $tipo_id = $avaiable->id;
            $tipo = $avaiable->nombre;
            $total = $avaiable->dias;
            }
            $final = $total - $usadas;
            if($final < 0  ){
                $final = 0;
            }
            }else{
                abort(404);
            }




      /*  $data['asignadas'] = $total;
        $data['usadas'] = $usadas;
        $data['disponibles'] = $final; */

        return view('home')->with([
            'total'     =>  $total,
            'usadas'     =>  $usadas,
            'final'     =>  $final,
            'canceladas' => count($cancelado)
        ]);



    }


}
