<?php

namespace App\Http\Controllers;

use App\Leave;
use Auth;
use App\Role;
use App\User;
use App\Oficina;
use App\Departamento;
use App\Puesto;
use App\Historial;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;



class LeavesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


      /**
     *  Search For Resource(s)
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request){
        $this->validate($request,[
            'search'   => 'required|min:1',
            'options'  => 'required'
        ]);
        $str = $request->input('search');
        $option = $request->input('options');
        $leaves = Leave::where($option, 'LIKE' , '%'.$str.'%')->Paginate(4);
        return view('leaves.index')->with(['leaves' => $leaves , 'search' => true ]);
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
       // $request->user()->authorizeRoles(['user', 'admin']);

        $leaves = Leave::get();

     return view('leaves.index')->with('leaves',$leaves);

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request->user()->authorizeRoles(['admin']);

        $users = User::orderBy('name','asc')->get();
        return view('leaves.create')->with([
            'users' => $users
        ]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'nombre' => 'required|min:4|unique:leaves',
            'dias' => 'required'
        ]);

        $leave = new Leave();
        $leave->nombre = $request->input('nombre');
        $leave->dias = $request->input('dias');
        $leave->save();

         return redirect('/leaves')->with('info','Tipo de Salida Creada!');
    }
     /**
     * Edit a existing resource
     *
     *  @param int
     * @return \Illuminate\Http\Response
     */
     public function edit($id){

        $leave = Leave::find($id);


        return view('leaves.edit')->with('leave' ,$leave);

     }
     /**
     * This method is used for validating the form
     *
     * @param  \Illuminate\Http\Request  $request
     * @return $this
     */
    private function validateRequest($request,$id){

            return $this->validate($request,[
            'nombre'     =>  'required|min:3|max:50',
            'dias'     =>  'required|min:1',
            ]);

        }
             /**
     * Edit a existing resource
     *
     *  @param \Illuminate\Http\Request
     *  @param  int  $id
     *  @return \Illuminate\Http\Response
     */
     public function update(Request $request, $id){

        $request->user()->authorizeRoles(['admin']);
        $this->validateRequest($request,$id);
        $leave = leave::find($id);
        $leave->nombre = $request->input('nombre');
        $leave->dias = $request->input('dias');
        $leave->save();

        return redirect('/leaves')->with('info','Se ha actualizado el departamento con exito!');

     }








}
