<?php

namespace App\Http\Controllers;


use App\Role;
use App\User;
use App\Oficina;
use App\Departamento;
use App\Puesto;
use App\Leaves;
use App\Historial;

use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;



class OficinasController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }



      /**
     *  Search For Resource(s)
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request){
        $this->validate($request,[
            'search'   => 'required|min:1',
            'options'  => 'required'
        ]);
        $str = $request->input('search');
        $option = $request->input('options');
        $oficinas = Oficina::where($option, 'LIKE' , '%'.$str.'%')->Paginate(4);
        return view('oficinas.index')->with(['oficinas' => $oficinas , 'search' => true ]);
    }


  /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*$oficina = Profile::with('oficina')->with('departamento')->with('puesto')->get();
        dd($oficina);*/
        $oficinas = Oficina::get();
        ///dd($oficina);
        return view('oficinas.index')->with('oficinas',$oficinas);

    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        /**
         *  Get Departments so we can show department
         *  name on the department dropdown in the view
         */

            /*$oficinas = Oficina::orderBy('nombre','asc')->get();
            $departmentos = Departamento::orderBy('nombre','asc')->get();
            $puestos = Puesto::orderBy('nombre','asc')->get();
            $roles = Role::get(); */
            /**
         *  this and other objects works the same as department
         */
       // $countries = Country::orderBy('country_name','asc')->get();
       // $cities = City::orderBy('city_name','asc')->get();
       // $states = State::orderBy('state_name','asc')->get();
       // $salaries = Salary::orderBy('s_amount','asc')->get();
       // $divisions = Division::orderBy('division_name','asc')->get();
       // $genders = Gender::orderBy('gender_name','asc')->get();
        /**
         *  return the view with an array of all these objects
         */
       /* return view('employee.create')->with([
            'departments'  => $departments,
            'countries'    => $countries,
            'cities'       => $cities,
            'states'       => $states,
            'salaries'     => $salaries,
            'divisions'    => $divisions,
            'genders'      => $genders
        ]);*/
        /* return view('employees.create')->with([
            'roles' => $roles,
            'oficinas' => $oficinas,
            'departamentos' => $departmentos,
            'puestos' => $puestos
        ]); */
            return view('oficinas.create');
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /**
         *  using laravel's pre-build validation class.
         *  it's first argument will be Request which is $request
         *  and second argument will be an array which will specify
         *  the validation rules.
         *  format is,
         *  'form field name' => 'rule'
         *  unique:departments means it should be unique to dept_name
         *  in departments table (note that input name and column name
         *  should be same)
         */
        $this->validate($request,[
            'nombre' => 'required|min:4|unique:oficinas',
            'domicilio' => 'required'
        ]);
        /**
         *  create new Department.
         *  add value(s) to it's fields.
         *  and save (store it to the database).
         */
        $oficina = new Oficina();
        $oficina->nombre = $request->input('nombre');
        $oficina->domicilio = $request->input('domicilio');
        $oficina->save();
        /**
         *  redirect us to the /departments route with a message.
         *  this message will make a toast that we have created in
         *  inc/alert view which is included in layouts/app view.
         *  see the inc/alert view
         */
         return redirect('/oficinas')->with('info','Oficina Creada!');
    }


     /**
     * Edit a existing resource
     *
     *  @param int
     * @return \Illuminate\Http\Response
     */

    public function edit($id){

        $oficina = Oficina::find($id);

        return view('oficinas.edit')->with('oficina' ,$oficina);

     }


       /**
     * Edit a existing resource
     *
     *  @param \Illuminate\Http\Request
     *  @param  int  $id
     *  @return \Illuminate\Http\Response
     */

     public function update(Request $request, $id){
        $request->user()->authorizeRoles(['admin']);

        $this->validateRequest($request,$id);

        $oficina = Oficina::find($id);

        $oficina->nombre = $request->input('nombre');
        $oficina->domicilio = $request->input('domicilio');
        $oficina -> save();

        return redirect('/oficinas')->with('info','Se ha actualizado la oficina con exito!');

     }



/**
     * This method is used for validating the form
     *
     * @param  \Illuminate\Http\Request  $request
     * @return $this
     */
    private function validateRequest($request,$id){

        return $this->validate($request,[
            'nombre'     =>  'required|min:3|max:50',
            'domicilio'     =>  'required|min:3|max:50',
            ]);

        }





}
