<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profiles';

    protected $fillable = ['user_id','departamento_id', 'puesto_id', 'oficina_id', 'jefe_id'];



  public function users (){
        return $this->belongsTo('App\User',  'user_id', 'id');
    }


    public function oficina()
    {
        return $this->belongsTo('App\Oficina', 'oficina_id', 'id');
    }

    public function departamento()
    {
        return $this->belongsTo('App\Departamento', 'departamento_id', 'id');
    }


    public function puesto()
    {
        return $this->belongsTo('App\Puesto', 'puesto_id', 'id');
    }


    public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }


    public function jefe(){
        return $this->belongsTo('App\User', 'jefe_id', 'id');
    }







}
