<?php

use App\User;
use App\Leave;
namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Notifications\Notifiable;


class Leave_user extends Model
{
    use notifiable;


    public function routeNotificationForMail($notification)
    {
        return $this->user->email;
    }

    protected $table = 'leave_users';
    protected $fillable = ['user_id', 'leaves_id', 'requested_from', 'requested_at', 'requested_days', 'status'];




    public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }


    public function leavez(){
        return $this->belongsTo('App\Leave', 'leave_id', 'id');
    }


}
