<?php

namespace App;
use App\Historial;
use App\Profile;
use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{

    protected $table = 'departamentos';

    public function dHistorial(){
        return $this->belongsTo('App\Historial');
    }


    public function dProfile(){
        return $this->belongsTo('App\Profile');
    }


}
