<?php

use App\Puesto;

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nivel extends Model{

    protected $table = 'niveles';


    public function puestos(){
        return $this->hasMany('App\Puesto', 'nivel_id', 'id');
    }
}
