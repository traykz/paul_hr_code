<?php

namespace App;
use App\Historial;
use App\Profile;

use Illuminate\Database\Eloquent\Model;

class Oficina extends Model
{

    protected $table = 'oficinas';

    public function oHistorial(){
        return $this->hasMany('App\Historial');
    }

    public function oProfiles(){
        return $this->hasMany('App\Profile');
    }


}
