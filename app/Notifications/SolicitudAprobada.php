<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

//use Illuminate\Notifications\Notifiable;


class SolicitudAprobada extends Notification
{
    use Queueable;

    public $data;
    public $e_info;
    public $pdf;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data, $e_info, $pdf)
    {
        $this->data = $data;
        $this->e_info = $e_info;
        $this->pdf = $pdf;
        //
    }


    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
       /* return (new MailMessage)
        ->greeting('Hola!')
        ->line('Tu solicitud ha sido aprobada!, descarga tu acuse')
        ->action( $notifiable->email, 'http://www.google.com')
        ->line('Te esperamos!'); */

        $data = $this->data;
        $e_info = $this->e_info;
        $pdf = $this->pdf;
        return (new MailMessage)
        ->attachData($this->pdf, 'recibo.pdf', ['mime' => 'application/pdf'])
        ->view('emails.email', ['email' => $notifiable->email, 'datos' => $data, 'user_info' => $e_info]
        );

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
