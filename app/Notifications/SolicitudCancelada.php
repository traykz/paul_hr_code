<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SolicitudCancelada extends Notification
{
    use Queueable;

    public $data;
    public $e_info;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data, $e_info)
    {
        $this->data = $data;
        $this->e_info = $e_info;
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
       /* return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');*/


                    $data = $this->data;
                    $e_info = $this->e_info;

                    return (new MailMessage)->view(
                        'emails.email', ['email' => $notifiable->email, 'datos' => $data, 'user_info' => $e_info]
                    );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
