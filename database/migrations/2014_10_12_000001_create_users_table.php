<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned()->unique()->index();
            $table->string('name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->string('gender');
            $table->string('phone');
            $table->mediumText('address');
            $table->date('join_date');
            $table->date('birth_date');
            $table->integer('age');
            $table->string('picture');


            $table->string('rfc');
            $table->string('curp');
            $table->string('estado_civil');
            $table->string('tipo_contrato');
            $table->string('is_custom_vacation')->nullable;
            $table->string('dias_vacacion')->nullable;



            $table->timestamps();
        });

        Schema::enableForeignKeyConstraints();


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
