<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeaveUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('leave_users', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('user_id')->unsigned()->nulladble();
            $table->bigInteger('leave_id')->unsigned()->nullable();
            $table->date('requested_from');
            $table->date('requested_at');
            $table->bigInteger('requested_days');
            $table->enum('status', ['pendiente','preaprobado','aceptado', 'cancelado']);
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('leave_id')->references('id')->on('leaves');
        });

        Schema::enableForeignKeyConstraints();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leave_users');
    }
}
