<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::disableForeignKeyConstraints();

        Schema::create('profiles', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->bigInteger('oficina_id')->unsigned()->nullable();
            $table->bigInteger('departamento_id')->unsigned()->nullable();
            $table->bigInteger('puesto_id')->unsigned()->nullable();
            $table->bigInteger('jefe_id')->unsigned()->nullable();
            $table->date('reasignado')->nullable();
            $table->date('baja_temp')->nullable();


            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('oficina_id')->references('id')->on('oficinas');
            $table->foreign('departamento_id')->references('id')->on('departamentos');
            $table->foreign('puesto_id')->references('id')->on('puestos');
            $table->foreign('jefe_id')->references('id')->on('users');


        });


          Schema::enableForeignKeyConstraints();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
