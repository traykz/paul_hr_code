<?php

use App\Puesto;

use Illuminate\Database\Seeder;

class PuestoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $puesto = new Puesto();
        $puesto->nombre = 'FrontEnd Developer Sr';
        $puesto->salario = 30000;
        $puesto->save();
    }
}
