<?php


use App\User;
use Carbon\Carbon;

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            // create a new admin when seeding
            $admin = new User();
            $admin->name = 'John';
            $admin->last_name = 'Doe';
            $admin->email = 'admin@admin.com';
            $admin->email_verified_at = Carbon::now()->format('Y-m-d H:i:s');
            $admin->password = bcrypt('password');
            $admin->gender = 'Masculino';
            $admin->phone = '55784390';
            $admin->address = 'Ote 65 2846';
            $admin->join_date = Carbon::now()->format('Y-m-d H:i:s');
            $admin->birth_date = Carbon::now()->format('Y-m-d H:i:s');
            $admin->age =  31;
            $admin->picture = 'no_image.png';
            $admin->rfc = 'GOCM880611';
            $admin->curp = 'GOCM880611HDFGR07';
            $admin->estado_civil = 'soltero';
            $admin->tipo_contrato = '1';
            $admin->is_custom_vacation = '';
            $admin->dias_vacacion = '';
            $admin->created_at = Carbon::now()->format('Y-m-d H:i:s');
            $admin->updated_at = Carbon::now()->format('Y-m-d H:i:s');
            $admin->save();
    }
}
