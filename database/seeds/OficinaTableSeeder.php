<?php

use Illuminate\Database\Seeder;

use App\Oficina;


class OficinaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $puesto = new Oficina();
        $puesto->nombre = 'Quibo Corporativo';
        $puesto->domicilio = 'Av Sta Fé 625';
        $puesto->save();
    }
}
