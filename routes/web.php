<?php


use App\Leave;
use App\Leave_user;
use App\User;
use App\Role;
use App\Oficina;
use App\Departamento;
use App\Puesto;
use App\Historial;
use App\Profile;
use App\Nivel;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('test', function () {
  $a = Puesto::with('pNivel')->get();


  //$puesto = Puesto::with('pnivel')->get();
  /*$nivel =  Nivel::with('puestos')->where('id', '=', 1)->get();
    $dropdown = '';
    foreach($nivel as $puestos){
       $dropdown = $puestos->puestos;
    }
    return response()->json($dropdown); */
    //  return view('welcome');
});


//Route::get('/sendmail', 'SolicitudesController@sendMail')->name('sendmail');

Route::get('examples', function (){
    $leaves = Leave::with('users')->get();

});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/pdf', 'SolicitudesController@pdf')->name('pdf');



Route::get('/dashboard','DashboardController@index')->name('dashboard');

Route::get('/logout','Auth\LoginController@logout')->name('logout');


/**
 *  States Route(s)
 */
Route::resource('/employees','EmployeesController');
Route::post('employees/search','EmployeesController@search')->name('employees.search');


Route::get('ajaxVacaciones', 'EmployeesController@ajaxVacaciones');
Route::post('ajaxVacaciones', 'EmployeesController@ajaxVacacionesPost');




Route::get('getNiveles/{any}', 'EmployeesController@getNiveles');
Route::post('getNiveles/{any}', 'EmployeesController@getNivelesPost');




/**
 *  Catalogos Oficinas - Departamentos - Puestos y Salarios
 */

Route::resource('/oficinas','OficinasController');
Route::post('oficinas/search','OficinasController@search')->name('oficinas.search');

Route::resource('/departamentos','DepartamentosController');
Route::post('departamentos/search','DepartamentosController@search')->name('departamentos.search');

Route::resource('/puestos','PuestosController');
Route::post('puestos/search','PuestosController@search')->name('puestos.search');

Route::resource('/leaves','LeavesController');
Route::post('leaves/search','LeavesController@search')->name('leaves.search');

Route::resource('/solicitudes','SolicitudesController');
Route::post('solicitudes/search','SolicitudesController@search')->name('solicitudes.search');

Route::post('solicitudes/{solicitude}/preaprobar','SolicitudesController@preaprobar')->name('solicitudes.preaprobar');
Route::get('solicitudes/{solicitude}/preaprobar','SolicitudesController@preaprobar')->name('solicitudes.preaprobar');

Route::post('solicitudes/{solicitude}/aceptar','SolicitudesController@aceptar')->name('solicitudes.aceptar');
Route::get('solicitudes/{solicitude}/aceptar','SolicitudesController@aceptar')->name('solicitudes.aceptar');

Route::post('solicitudes/{solicitude}/rechazar','SolicitudesController@rechazar')->name('solicitudes.rechazar');
Route::get('solicitudes/{solicitude}/rechazar','SolicitudesController@rechazar')->name('solicitudes.rechazar');


/* obtiene json asignadas, usadas, disponibles*/
Route::post('solicitudes/getSalidas/{any}','SolicitudesController@getSalidas')->name('solicitudes.getSalidas');
Route::get('solicitudes/getSalidas/{any}','SolicitudesController@getSalidas')->name('solicitudes.getSalidas');


/*Vista para conocer tus propias Vacaciones*/
Route::post('getSalidas','HomeController@index')->name('Home.getSalidas');
Route::get('getSalidas','HomeController@index')->name('Home.getSalidas');



