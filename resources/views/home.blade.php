@extends('layouts.app')

                <?php use App\User ;

                    $user = User::with('roles')->find(Auth::user()->id);
                    $rol =  $user->roles;

                    foreach($rol as $r){
                     $rolling = $r->name;
                    }

                ?>


@section('content')


    <br>
    <div>


        <div class="row white-text">

            <a href="#" class="white-text">
                <div class="mx-20 card-panel blue lighten-1 col s18 offset-s2 m4 offset-m2 l4 offset-l2 xl2 offset-xl1 ml-14">
                    <div class="row">
                        <div class="col s7 m7 xl7">
                            <i class="material-icons medium white-text pt-5">perm_contact_calendar
                                </i>
                            <h6 class="no-padding txt-md">Vacaciones por Contrato</h6>
                        </div>
                        <div class="col s5 m5 xl5 ">
                        <h3 class="no-padding center mt">{{$total}} </h3>
                        </div>
                    </div>
                </div>
            </a>
            <a href="#" class="white-text">
                <div class="mx-20 card-panel teal lighten-1 col s8 offset-s2 m4 l4 xl2">
                    <div class="row">
                        <div class="col s7 xl7">
                            <i class="material-icons medium white-text pt-5">check</i>
                            <h6 class="no-padding txt-md">Vacaciones Aprobadas</h6>
                        </div>
                        <div class="col s5 xl5">
                            <h3 class="no-padding center mt "> {{$usadas}}</h3>
                        </div>
                    </div>
                </div>
            </a>
            <a href="#" class="white-text">
                <div class="mx-20 card-panel orange lighten-1 col s8 offset-s2 m4 offset-m2 l4 offset-l2 xl2">
                    <div class="row">
                        <div class="col s7 xl7">
                            <i class="material-icons medium white-text pt-5">card_travel</i>
                            <h6 class="no-padding txt-md">Tienes Disponibles</h6>
                        </div>
                        <div class="col s5 xl5">
                        <h3 class="no-padding center mt ">{{$final}} </h3>
                        </div>
                    </div>
                </div>
            </a>
            <a href="#" class="white-text hide-on-small-only">
                <div class="mx-20 card-panel red lighten-1 col s8 offset-s2 m4 l4 xl2">
                    <div class="row">
                        <div class="col s7 xl7">
                            <i class="material-icons medium white-text pt-5">cancel</i>
                            <h6 class="no-padding txt-md">Vacaciones Rechazadas</h6>
                        </div>
                        <div class="col s5 xl5">
                        <h3 class="no-padding center mt "> {{$canceladas}}</h3>
                            </div>
                    </div>
                </div>
            </a>


        </div>
    </div>
   <!--  <div class="container-fluid">
        <div class="card-panel">
           <canvas id="employee"></canvas>
        </div>
    </div>  -->
    <br>


<div class="container">


        <div class="row">
                <div class="col s12 m6">
                  <div class="card">
                    <div class="card-image">
                      <img src="{{asset('img/plane.jfif')}}">
                      <span class="card-title">Mis Vacaciones</span>
                      <a href="{{route('solicitudes.create')}}" class="btn-floating halfway-fab waves-effect waves-light red"><i class="material-icons">forward</i></a>
                    </div>
                    <div class="card-content">
                      <p>Solicita tus Vacaciones, desde nuestro portal, si es Aprobada o Rechazada, te lo haremos saber automáticamente a tu correo electrónico.</p>
                    </div>
                  </div>
                </div>
                <div class="col s12 m6">
                        <div class="card">
                          <div class="card-image">
                            <img width="50%" src="{{asset('img/profil.jpg')}}">
                            <span class="card-title">Mi Perfil</span>
                            <a href="{{route('solicitudes.create')}}" class="btn-floating halfway-fab waves-effect waves-light red"><i class="material-icons">visibility</i></a>
                          </div>
                          <div class="card-content">
                            <p>Revisa, tus datos personales y laborales a lo largo de tu tiempo en BLM.</p>
                          </div>
                        </div>
                      </div>
              </div>

       <!-- <div class="fixed-action-btn">
                <a class="btn-floating btn-large waves=effect waves-light red" href="{{route('employees.create')}}">
                    <i class="large material-icons">add</i>
                </a>
            </div>-->

           <!-- <div class="row">
                    <div class="card col s12 m12 l12 xl12 mt-5">
                        <div>
                        <h4 class="center grey-text text-darken-2 card-title">Mi Perfil</h4>
                        </div>
                        <hr>
                        <div class="card-content">

                        </div>
                    </div>
                </div> -->

</div>





@endsection
