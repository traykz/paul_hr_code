



     <div class="row collection mt-20">
                    <div class="hide-on-med-only hide-on-large-only row">
                        <div class="col s8 offset-s2 mt-20">
                            <img class="p5 card-panel emp-img-big" src="{{asset('img/bluelabel.png')}}">
                        </div>
                    </div>

                    <div class="col m12 l8 xl8 center">
                        <h1 class="grey-text text-darken-1 center">Solicitud  de Salida #{{$datos['id']}} -

                                <?php $status = ''; ?>

                                    @if($datos['status'] === 1)
                                    Pendiente
                                    @endif

                                    @if($datos['status'] === 2)
                                    PreAprobada, Favor de Imprimir tu recibo
                                    @endif

                                    @if($datos['status'] === 3)
                                    Aceptada
                                    @endif

                                    @if($datos['status'] === 4)
                                    Rechazada
                                    @endif



                        </h1>
                        <p class="pl-15 mt-20"><b>Fecha de Inicio:</b> {{$datos['requested_from']}} - <b>Fecha Final:</b> {{$datos['requested_at']}}  </p>
                        <h2 class="pl-15 mt-20">{{$datos['user']['name']}} {{$datos['user']['last_name']}}</h2>
                        <h5>{{$user_info[0]['profileoficinas'][0]['nombre']}}, {{$user_info[0]['profiledepartamentos'][0]['nombre']}}, {{$user_info[0]['profilepuestos'][0]['nombre']}}.</h5>
                        <p class="pl-15 mt-20">Días Solicitados: {{$datos['requested_days']}}  </p>
                        <p class="pl-15 mt-20">Imprime tu Acuse del documentos adjunto </p>


                    </div>

                </div>
