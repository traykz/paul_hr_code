@extends('layouts.app')

@push('head')

<style>
table.dataTable tbody th,
table.dataTable tbody td {
    white-space: nowrap;
}

    </style>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>

<!-- Styles -->
<link href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">
<!-- Scripts -->

@endpush


@section('content')
<div class="container">
    <div class="card">
         <div class="card-content">
                <h5 class="grey-text text-darken-1 center">Empleados</h5>
            <div class="row ">
                <table width="100%" id="empleados" class="table-responsive display ">
                    <thead class="grey-text text-darken-1">
                        <tr>
                            <th>ID</th>
                            <th>Avatar</th>
                            <th>Nombre</th>
                            <th>Rol</th>
                            <th>Departamento</th>
                            <th>Area</th>
                            <th>Jefe Directo</th>
                            <th>Ingreso</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody id="emp-table">
                        <!-- Check if there are any employee to render in view -->
                        @if($employees->count())
                            @foreach($employees as $employee)
                                <tr>
                                    <td>{{$employee->id}}</td>
                                    <td>
                                    <img class="emp-img" src="{{asset('storage/employee_images/'.$employee->picture)}}">
                                    </td>
                                    <td>{{$employee->name}} {{$employee->last_name}}</td>
                                    <td>


                                        @foreach ($employee->roles as $rol )

                                       {{$rol->name}}
                                                                           @endforeach



                                    </td>
                                    <td>
                                        @foreach ($employee->profileoficinas as $o )

                                        {{$o->nombre}}
                                                                            @endforeach
                                    </td>
                                  <td>
                                    @foreach ($employee->profiledepartamentos as $d )
                                    {{$d->nombre}}
                                    @endforeach
                                   / @foreach ($employee->profilepuestos as $p )
                                    {{$p->nombre}}
                                    @endforeach
                                  </td>

                                  <td>
                                        @foreach ($employee->profilejefes as $p )

                                        {{$p->name}} {{$p->last_name}}
                                                                            @endforeach

                                  </td>
                                    <td>{{$employee->join_date}}</td>
                                    <td>
                                    <a href="{{route('employees.show',$employee->id)}}" class="btn btn-small btn-floating waves=effect waves-light teal lighten-2"><i class="material-icons">list</i></a>
                                    </td>
                                </tr>

                            @endforeach

                        @else


                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Card END -->
</div>
<div class="fixed-action-btn">
    <a class="btn-floating btn-large waves=effect waves-light red" href="{{route('employees.create')}}">
        <i class="large material-icons">add</i>
    </a>
</div>



@push('script')

<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>


<script>

$(document).ready( function () {
    $('#empleados').DataTable({
        "order": [[ 0, "desc" ]],
          "scrollX": true

    });


} );


</script>


@endpush

@endsection
