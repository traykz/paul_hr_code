@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card-panel grey-text text-darken-2 mt-20">
            <div class="row">
                <div class="row collection mt-20">
                        <h5 class="grey-text text-darken-1 center">Detalles del Empleado</h5>
                    <!-- Show this image on small devices -->
                    <div class="hide-on-med-only hide-on-large-only row">
                        <div class="col s8 offset-s2 mt-20">
                            <img class="p5 card-panel emp-img-big" src="{{asset('storage/employee_images/'.$profile->picture)}}">
                        </div>
                    </div>
                    <div class="col m8 l8 xl8">
                        <h5 class="pl-15 mt-20">{{$profile->name}} {{$profile->last_name}}</h5>
                        <p class="pl-15 mt-20"><i class="material-icons left">location_on</i>{{$profile->address}}</p>
                        <p class="pl-15 mt-20"><i class="material-icons left">cake</i>{{$profile->birth_date}} ({{$profile->age}} años) </p>
                        <p class="pl-15"><i class="material-icons left">perm_phone_msg</i>{{$profile->phone}}</p>
                        <p class="pl-15"><i class="material-icons left">email</i>{{$profile->email}}</p>
                    </div>
                    <!-- Hide this image on small devices -->
                    <div class="hide-on-small-only col m4 l4 xl3">
                        <img class="p5 card-panel emp-img-big" src="{{asset('storage/employee_images/'.$profile->picture)}}">
                    </div>
                </div>
                <div class="collection">
                        <h5 class="grey-text text-darken-1 center">Posición Actual</h5>
                    <div class="row">
                        <p class="pl-15"><span class="bold col s5 m4 l4 xl3">Oficina :</span><span class="col m8 l8 xl9">
                                 @foreach($profile->profileoficinas as $oficina)
                                  {{$oficina->nombre}}
                                 @endforeach
                        </span></p>
                    </div>
                    <div class="row">
                        <p class="pl-15"><span class="bold col s5 m4 l4 xl3">Departamento :</span><span class="col m8 l8 xl9">
                                @foreach($profile->profiledepartamentos as $departamento)
                                {{$departamento->nombre}}
                                @endforeach
                        </span></p>
                    </div>
                    <div class="row">
                            <p class="pl-15"><span class="bold col s5 m4 l4 xl3">Puesto :</span><span class="col m8 l8 xl9">
                                    @foreach($profile->profilepuestos as $puesto)
                                    {{$puesto->nombre}}
                                    @endforeach
                            </span></p>
                        </div>
                    <div class="row">
                        <p class="pl-15"><span class="bold col s5 m4 l4 xl3">Rol :</span><span class="col m8 l8 xl9">
                                @foreach($profile->roles as $rol)
                                {{$rol->name}}
                                 @endforeach
                        </span></p>
                    </div>
                    <div class="row">
                        <p class="pl-15"><span class="bold col s5 m4 l4 xl3">Salario Actual :</span><span class="col m8 l8 xl9">
                            @foreach($profile->zalario as $zalario)
                            {{$zalario->salario}}
                             @endforeach

                        </span></p>
                    </div>
                    <div class="row">
                        <p class="pl-15"><span class="bold col s5 m4 l4 xl3">Fecha de Ingreso :</span><span class="col m8 l8 xl9">{{$profile->join_date}}</span></p>
                    </div>
                </div>

                <div class="collection">
                        <h5 class="grey-text text-darken-1 center">Historico de Posiciones</h5>
                    <div class="row">
                            <!-- Table that shows Employee List -->
                            <table class="responsive-table col s12 m12 l12 xl12">
                                <thead class="grey-text text-darken-1">
                                    <tr>
                                        <th>Oficina/Posición</th>
                                        <th>Nivel</th>
                                        <th>Salario</th>
                                        <th>Jefe Directo</th>
                                        <th>Resignado</th>
                                        <th>Baja x Reasignación</th>
                                    </tr>
                                </thead>
                                <tbody id="emp-table">
                                    <!-- Check if there are any employee to render in view -->
                                    @if(count($nivel))


                                    @foreach($nivel as $n)

                                    <tr>
                                        <td>
                                            {{$n->oficina_nombre}}
                                            <br>
                                            {{$n->depa_nombre}}
                                            <br>
                                            {{$n->puesto_nombre}}

                                        </td>

                                        <td>
                                            {{$n->nivel_nombre}}
                                        </td>
                                        <td>
                                            $ {{ number_format($n->sueldo, 2, '.', ',') }}


                                        </td>
                                        <td>
                                            {{$n->jefe_nombre}} {{$n->jefe_apellido}}
                                        </td>
                                        <td>
                                            {{$n->reasignado}}
                                         </td>

                                         <td>
                                            {{$n->baja_temp}}
                                         </td>

                                    </tr>

                                     @endforeach
                                    @if(isset($search))
                                            <tr>
                                                <td colspan="4">
                                                    <a href="/employees" class="right">Ver Todos</a>
                                                </td>
                                            </tr>
                                        @endif
                                    @else
                                        {{-- if there are no employees then show this message --}}
                                        <tr>
                                            <td colspan="5"><h6 class="grey-text text-darken-2 center">No se encontraron registros!</h6></td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                            <!-- employees Table END -->
                    </div>

                </div>



                <form action="{{route('employees.destroy',$profile->id)}}" method="POST">
                    @method('DELETE')
                    @csrf()
                    <button class="btn red col s3 offset-s2 m3 offset-m2 l3 offset-l2 xl3 offset-xl2" type="submit">Baja</button>
                </form>
                <a class="btn orange col s3 offset-s2 m3 offset-m2 l3 offset-l2 xl3 offset-xl2" href="{{route('employees.edit',$profile->id)}}">Actualizar</a>
            </div>
        </div>
    </div>
@endsection
