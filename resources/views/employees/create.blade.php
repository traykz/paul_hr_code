@extends('layouts.app')

@push('head')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>

@endpush


@section('content')
    <div class="container">
        <div class="row">
            <div class="card col s12 m12 l12 xl12 mt-20">
                <div>
                <h4 class="center grey-text text-darken-2 card-title">Nuevo Empleado</h4>
                </div>
                <hr>
                <div class="card-content">
                    <form action="{{route('employees.store')}}" method="POST" enctype="multipart/form-data">
                        <div class="row">
                            <div class="input-field col s12 m6 l6 xl4 offset-xl2">
                                <i class="material-icons prefix">person</i>
                                <input type="text" name="name" id="name" value="{{Request::old('name') ? : ''}}">
                                <label for="name">Nombre</label>
                                <span class="{{$errors->has('name') ? 'helper-text red-text' : ''}}">{{$errors->first('name')}}</span>
                            </div>
                            <div class="input-field col s12 m6 l6 xl4">
                                <i class="material-icons prefix">person</i>
                                <input type="text" name="last_name" id="last_name" value="{{Request::old('last_name') ? : ''}}">
                                <label for="last_name">Apellido</label>
                                <span class="{{$errors->has('last_name') ? 'helper-text red-text' : ''}}">{{$errors->first('last_name')}}</span>
                            </div>
                            <div class="input-field col s12 m6 l6 xl8 offset-xl2">
                                <i class="material-icons prefix">email</i>
                                <input type="email" name="email" id="email" value="{{Request::old('email') ? : ''}}">
                                <label for="email">Email</label>
                                <span class="{{$errors->has('email') ? 'helper-text red-text' : ''}}">{{$errors->has('email') ? $errors->first('email') : ''}}</span>
                            </div>



                            <div class="input-field col s12 m6 l6 xl4 offset-xl2">
                                <i class="material-icons prefix">contact_phone</i>
                                <input type="number" name="phone" id="phone" value="{{Request::old('phone') ? : ''}}">
                                <label for="phone">Teléfono</label>
                                <span class="{{$errors->has('phone') ? 'helper-text red-text' : ''}}">{{$errors->has('phone') ? $errors->first('phone') : ''}}</span>
                            </div>


                            <div class="input-field col s12 m6 l6 xl4">
                                <i class="material-icons prefix">lock</i>
                                <input type="password" name="password" id="password" value="{{Request::old('password') ? : ''}}">
                                <label for="password">Password</label>
                                <span class="{{$errors->has('password') ? 'helper-text red-text' : ''}}">{{$errors->has('password') ? $errors->first('phone') : ''}}</span>
                            </div>

                            <div class="input-field col s12 m8 l8 xl8 offset-xl2">
                                    <i class="material-icons prefix">lock</i>
                                    <input type="password" name="password_confirmation" id="password_confirmation" value="{{Request::old('password_confirmation') ? : ''}}">
                                    <label for="password_confirmation">password_confirmation</label>
                                    <span class="{{$errors->has('password_confirmation') ? 'helper-text red-text' : ''}}">{{$errors->has('password_confirmation') ? $errors->first('password') : ''}}</span>
                                </div>

                            <div class="input-field col s12 m6 l6 xl8 offset-xl2">
                                <i class="material-icons prefix">add_location</i>
                                <textarea name="address" id="address" class="materialize-textarea" >{{Request::old('address') ? : ''}}</textarea>
                                <label for="address">Dirección</label>
                                <span class="{{$errors->has('address') ? 'helper-text red-text' : ''}}">{{$errors->has('address') ? $errors->first('address') : ''}}</span>
                            </div>


                            <div class="input-field col s12 m6 l6 xl4 offset-xl2">
                                    <i class="material-icons prefix">perm_identity</i>
                                    <input type="text" name="rfc" id="rfc" value="{{Request::old('rfc') ? : ''}}">
                                    <label for="rfc">RFC</label>
                                    <span class="{{$errors->has('rfc') ? 'helper-text red-text' : ''}}">{{$errors->has('rfc') ? $errors->first('rfc') : ''}}</span>
                                </div>
                            <div class="input-field col s12 m6 l6 xl4">
                                    <i class="material-icons prefix">perm_identity</i>
                                    <input type="text" name="curp" id="curp" value="{{Request::old('curp') ? : ''}}">
                                    <label for="curp">CURP</label>
                                    <span class="{{$errors->has('curp') ? 'helper-text red-text' : ''}}">{{$errors->has('curp') ? $errors->first('curp') : ''}}</span>
                            </div>

                            <div class="input-field col s12 m6 l6 xl4 offset-xl2">
                                    <i class="material-icons prefix">perm_identity</i>
                                    <input type="text" name="tipo_contrato" id="tipo_contrato" value="{{Request::old('tipo_contrato') ? : ''}}">
                                    <label for="tipo_contrato">Tipo de Contrato</label>
                                    <span class="{{$errors->has('tipo_contrato') ? 'helper-text red-text' : ''}}">{{$errors->has('tipo_contrato') ? $errors->first('tipo_contrato') : ''}}</span>
                                </div>
                            <div class="input-field col s12 m6 l6 xl4">
                                    <i class="material-icons prefix">perm_identity</i>
                                    <input type="text" name="estado_civil" id="estado_civil" value="{{Request::old('estado_civil') ? : ''}}">
                                    <label for="estado_civil">Estado Civil</label>
                                    <span class="{{$errors->has('estado_civil') ? 'helper-text red-text' : ''}}">{{$errors->has('estado_civil') ? $errors->first('estado_civil') : ''}}</span>
                            </div>


                            <div class="input-field col s12 m6 l6 xl4 offset-xl2">
                                <i class="material-icons prefix">perm_identity</i>
                                <input type="number" name="age" id="age" value="{{Request::old('age') ? : ''}}">
                                <label for="age">Edad</label>
                                <span class="{{$errors->has('age') ? 'helper-text red-text' : ''}}">{{$errors->has('age') ? $errors->first('age') : ''}}</span>
                            </div>

                            <div class="input-field col s12 m6 l6 xl4">
                            <i class="material-icons prefix">date_range</i>
                            <input type="text" name="birth_date" id="birth_date" class="datepicker" value="{{Request::old('birth_date') ? : ''}}">
                            <label for="birth_date">Fecha de Nacimiento</label>
                            <span class="{{$errors->has('birth_date') ? 'helper-text red-text' : ''}}">{{$errors->has('birth_date') ? $errors->first('birth_date') : '' }}</span>
                        </div>

                        <div class="input-field col s12 m6 l6 xl4 offset-xl2">
                                <i class="material-icons prefix">person_outline</i>
                            <select name="gender">
                                <option value="" disabled {{ old('gender')? '' : 'selected' }}>Selecciona</option>
                                <option value="Masculino" {{ old('gender')? 'selected' : '' }}>Masculino</option>
                                <option value="Femenino" {{ old('gender')? 'selected' : '' }}>Femenino</option>
                            </select>
                        <label for="gender">Genero</label>
                        </div>

                            <div class="input-field col s12 m6 l6 xl4">
                                    <i class="material-icons prefix">date_range</i>
                                <input type="text" name="join_date" id="join_date" class="datepicker" value="{{old('join_date') ? : ''}}">
                                <label for="join_date">Fecha de Ingreso</label>
                                <span class="{{$errors->has('join_date') ? 'helper-text red-text' : ''}}">{{$errors->has('join_date') ? $errors->first('join_date') : ''}}</span>
                            </div>


                            <div class="input-field col s12 m6 l6 xl4 offset-xl2">
                                    <i class="material-icons prefix">person_outline</i>
                                    <select name="oficina">
                                            <option value="" disabled {{ old('oficina')? '' : 'selected' }}>Selecciona</option>
                                            @foreach($oficinas as $oficina)
                                                <option value="{{$oficina->id}}" {{ old('oficina')? 'selected' : '' }}>{{$oficina->nombre}}</option>
                                            @endforeach
                                    </select>
                                    <label>Oficina</label>
                                </div>

                                <div class="input-field col s12 m6 l6 xl4">
                                        <i class="material-icons prefix">person_outline</i>
                                        <select name="departamento">
                                                <option value="" disabled {{ old('departamento')? '' : 'selected' }}>Selecciona</option>
                                                @foreach($departamentos as $departamento)
                                                    <option value="{{$departamento->id}}" {{ old('departamento')? 'selected' : '' }}>{{$departamento->nombre}}</option>
                                                @endforeach
                                        </select>
                                        <label>Departamento</label>
                                    </div>




                                    <div class="input-field col s12 m6 l6 xl4 offset-xl2">
                                        <i class="material-icons prefix">person_outline</i>
                                         <select name="role">
                                            <option value="" disabled {{ old('role')? '' : 'selected' }}>Selecciona</option>
                                            @foreach($roles as $role)
                                                <option value="{{$role->id}}" {{ old('role')? 'selected' : '' }}>{{$role->name}}</option>
                                            @endforeach
                                         </select>
                                         <label>Rol</label>
                                      </div>

                                      <div class="input-field col s12 m6 l6 xl4">
                                            <i class="material-icons prefix">person_outline</i>
                                                        <select name="jefe">
                                                                <option value="" disabled {{ old('jefe')? '' : 'selected' }}>Selecciona</option>

                                                                @if($jefes)
                                                                @foreach($jefes as $user)
                                                                        @foreach($user->users as $usex){
                                                                        <option value="{{$usex->id}}" {{old('jefe') ? 'selected' : '' }} >{{$usex->name}} {{$usex->last_name}}</option>
                                                                         @endforeach
                                                                @endforeach
                                                                @endif



                                                        </select>
                                                        <label>Jefe</label>
                                                    </div>



                                                    <div class="input-field col s12 m6 l6 xl4 offset-xl2">
                                                        <i class="material-icons prefix">person_outline</i>
                                                         <select id="sel_nivel" name="nivel">
                                                            <option value="" disabled {{ old('nivel')? '' : 'selected' }}>Selecciona</option>
                                                            @foreach($niveles as $nivel)
                                                                <option value="{{$nivel->id}}" {{ old('nivel')? 'selected' : '' }}>{{$nivel->nombre}}</option>
                                                            @endforeach
                                                         </select>
                                                         <label>Nivel</label>
                                                      </div>

                                                      <div class="input-field col s12 m6 l6 xl4">
                                                            <i class="material-icons prefix">person_outline</i>


                                            <select id="sel_puesto" name="puesto">

                                            </select>
                                            <label>Puesto</label>




                                                      </div>






                                        <div class="input-field col s12 m6 l6 xl4 offset-xl2">
                                            <i class="material-icons prefix">attach_money</i>
                                            <input type="number" name="salario" id="salario" value="{{Request::old('salario') ? : ''}}">
                                            <label for="salario">Salario</label>
                                            <span class="{{$errors->has('salario') ? 'helper-text red-text' : ''}}">{{$errors->has('salario') ? $errors->first('salario') : ''}}</span>

                                        </div>



                                        <div class="input-field col s12 m6 l6 xl4">

                                        <label>
                                                <input type="checkbox" id="custom"/>
                                                <span>Días de Vacaciones Personalizado</span>
                                              </label>
                                        </div>

                            <div class="input-field col s12 m6 l6 xl6 offset-xl2">
                                                <i class="material-icons prefix">person_outline</i>
                                <select name="nivel_salidas" id="nivel_salidas">
                                          <option value="" disabled {{ old('nivel_salidas')? '' : 'selected' }}>Selecciona</option>
                                        @foreach($nivel_salidas as $nivel_salida)
                                    <span>Avatar</span>
                                            <option value="{{$nivel_salida->id}}" {{ old('nivel_salidas')? 'selected' : '' }}>{{$nivel_salida->nombre}} -> {{$nivel_salida->dias}} Días</option>
                                        @endforeach
                                </select>
                                <label>Salidas por Nivel</label>

                            </div>


                            <div id="wrapper"> </div>

                            <div class="file-field input-field col s12 m12 l12 xl8 offset-xl2">
                                <div class="btn">
                                    <span>Avatar</span>
                                    <input type="file" name="picture">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text" value="{{old('picture') ? : '' }}">
                                    <span class="{{$errors->has('picture') ? 'helper-text red-text' : ''}}">{{$errors->has('picture') ? $errors->first('picture') : ''}}</span>
                                </div>
                            </div>
                        </div>
                        @csrf()
                        <div class="row">
                            <button type="submit" class="btn waves-effect waves-light col s8 offset-s2 m4 offset-m4 l4 offset-l4 xl4 offset-xl4">Agregar Empleado</button>
                        </div>
                    </form>
                </div>
                <div class="card-action">
                    <a href="/employees">Regresar</a>
                </div>
            </div>
        </div>
    </div>



    @push('script')



<script type="text/javascript">
    $(document).ready( function () {
        $.ajaxSetup({
            headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}",
            }
        });
        $("#custom").on('change', function() {

            var wrapper  = $("#wrapper"); //Fields wrapper
            if ($(this).is(':checked')) {
                checkFlag = 'checked';
               // $('#custom').attr('disabled', true)
                $('#nivel_salidas').prop('disabled', true);
                $("#nivel_salidas").formSelect();
                $(wrapper).append('<div id="remove"  class="input-field col s12 m6 l6 xl6 offset-xl2"><i class="material-icons prefix">perm_identity</i><input type="number" name="vacaciones"/><label for="dias">Dias de Vacaciones Personalizadas</label></div>'); //add input box
            }else{
                var checkFlag = 'unchecked';
               // $('#custom').attr('disabled', false)
                $('#nivel_salidas').prop('disabled', false);

                $("#nivel_salidas").formSelect();

                $('#remove').remove();

            }

            $.ajax({
                type:'POST',
                url:'/ajaxRequest',
                data:{checkbox:checkFlag},
                success:function(data){
                alert(data.success);
                }
          });
});


 // Department Change
 $('#sel_nivel').change(function(){

// Department id
var id = $(this).val();

// Empty the dropdown
//$('#sel_puesto').find('option').not(':first').remove();
$('#sel_puesto').find('option').remove();
// AJAX request
$.ajax({

  type : 'GET',
  url: '{{url("getNiveles")}}/'+id,
  dataType: 'json',
  success: function(response){

    var len = 0;
    if(response != null){
      len = response.length;
    }

    if(len > 0){
            for(var i=0; i<len; i++){
        var id = response[i].id;
        var name = response[i].nombre;

        var option = "<option value='"+id+"'>"+name+"</option>";
        console.log(option);
        $("#sel_puesto").append(option);
        $("#sel_puesto").formSelect();

      }
    }

  }
});
});

















});
    </script>


    @endpush



@endsection
