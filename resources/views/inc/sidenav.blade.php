<ul id="slide-out" class="sidenav sidenav-fixed grey lighten-4">
    <li>
        <div class="user-view">
            <div class="background">
            </div>
            {{-- Get picture of authenicated user --}}
          <!--  <a href=""><img class="circle" src="asset('storage/admins/'.Auth::user()->picture)}}"></a>-->
            <a href=""><img class="square" src="http://www.bluelabel.mx/wp-content/uploads/2018/01/logo_blue_label_blanco_204.png"></a>
            {{-- Get first and last name of authenicated user --}}
            <a href=""><span class="white-text name">


                    @if(Auth::check())

                    {{ Auth::user()->name }} {{ Auth::user()->last_name }}

                    @endif


            </span></a>
            {{-- Get email of authenicated user --}}
            <a href=""><span class="white-text email">
                    @if(Auth::check())
                    {{ Auth::user()->email }}
                    @endif

                    <?php use App\User ;
                    $user = User::with('roles')->find(Auth::user()->id);
                    $rol =  $user->roles;
                    foreach($rol as $r){
                     $rolling = $r->name;
                    }
                    ?>

                    @if($rolling == 'user' )
                       ({{ $rolling }})
                    @endif
                </span></a>


        </div>
    </li>

    @if($rolling == 'admin' )

    <li>
        <a class="waves-effect waves-grey" href="/dashboard"><i class="material-icons">dashboard</i>Dashboard</a>
    </li>

    @endif


    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
            <li>
                <a class="collapsible-header"><i class="material-icons pl-15">post_add
                </i><span class="pl-15">Solicitudes</span></a>
                <div class="collapsible-body">

                    @if($rolling == 'admin' )

                    <ul>
                        <li>
                            <a href="/solicitudes" class="waves-effect waves-grey">
                               <i class="material-icons">directions_run</i>
                                Salidas
                            </a>
                       </li>
                    </ul>

                    @endif

                    @if($rolling == 'user' )
                    <ul>
                        <li>
                                <a href="/solicitudes/create" class="waves-effect waves-grey">
                                   <i class="material-icons">directions_run</i>
                                    Nuevo Solicitud de Salida
                                </a>
                        </li>
                    </ul>
                    <ul>
                            <li>
                                <a href="/solicitudes" class="waves-effect waves-grey">
                                       <i class="material-icons">directions_run</i>
                                        Mis Salidas
                                 </a>
                            </li>
                        </ul>
                    @endif

                    <ul>
                        <li>
                            <a href="#" class="waves-effect waves-grey">
                               <i class="material-icons">monetization_on
                            </i>
                                Viaticos (Próximamente).
                            </a>
                       </li>
                    </ul>
                </div>
            </li>
        </ul>
    </li>

    @if($rolling == 'admin' )

    <li>
        <a class="waves-effect waves-grey" href="/employees"><i class="material-icons">supervisor_account</i>Gestión Empleados</a>
    </li>

    @endif

    @if($rolling == 'admin' )

    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
            <li>
                <a class="collapsible-header"><i class="material-icons pl-15">settings</i><span class="pl-15">Configuración</span></a>
                <div class="collapsible-body">
                    <ul>

                        <li>
                              <a href="/reports" class="waves-effect waves-grey">
                                        <i class="material-icons">insert_drive_file</i>
                                        Reportes
                                    </a>
                                </li>
                        <li>
                             <a href="/leaves" class="waves-effect waves-grey">
                                <i class="material-icons">beach_access</i>
                                Vacaciones
                             </a>
                        </li>
                        <li>
                            <a href="/oficinas" class="waves-effect waves-grey">
                                <i class="material-icons">business</i>
                                Oficinas
                            </a>
                        </li>

                        <li>
                            <a href="/departamentos" class="waves-effect waves-grey">
                            <i class="material-icons">business</i>
                                Departamentos/Areas
                            </a>
                        </li>
                        <li>
                            <a href="/puestos" class="waves-effect waves-grey">
                                    <i class="material-icons">attach_money</i>
                                    Puestos y Salarios
                            </a>
                        </li>
                      <!--  <li>
                            <a href="/cities" class="waves-effect waves-grey">
                            <i class="material-icons">location_city</i>
                                Ciudades
                            </a>
                        </li>
                        <li>
                            <a href="/states" class="waves-effect waves-grey">
                            <i class="material-icons">grid_on</i>
                                Estados
                            </a>
                        </li>
                        <li>
                            <a href="/countries" class="waves-effect waves-grey">
                            <i class="material-icons">terrain</i>
                                Países
                            </a>
                        </li>-->

                    </ul>
                </div>
            </li>
        </ul>
    </li>

    @endif


</ul>
