@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="card col s12 m8 offset-m2 l8 offset-l2 xl8 offset-xl2 card-mt-15">



                <h4 class="center grey-text text-darken-2 card-title">Editar Puesto</h4>
                <div class="card-content">
                    <div class="row">
                            <form action="{{route('puestos.update', $oficina->id)}}" method="POST" >
                            <div class="input-field">
                            <i class="material-icons prefix">account_balance</i>
                                <input type="text" name="nombre" id="nombre" class="validate" value="{{ Request::old('nombre') ? : $oficina->nombre}} ">
                                <label for="nombre">Nombre de Puesto</label>
                                <span class="{{$errors->has('nombre') ? 'helper-text red-text' : '' }}">{{$errors->first('nombre')}}</span>
                            </div>
                            <div class="input-field">
                                <i class="material-icons prefix">account_balance</i>
                                  <select name="nivel">

                                    @foreach($niveles as $level)

                                  <option value="{{$level->id}}"   {{  ( $oficina->nivel_id === $level->id ) ? 'selected="selected"'  : '' }} >{{$level->nombre}} </option>

                                    @endforeach

                                  </select>
                                </div>

                                @method('PUT')
                                @csrf()
                            <button type="submit" class="btn waves-effect waves-light mt-15 col s6 offset-s3 m4 offset-m4 l4 offset-l4 xl4-offset-xl4">Actualizar</button>
                        </form>
                    </div>
                </div>
                <div class="card-action">
                    <a href="/oficinas">Regresar</a>
                </div>
            </div>
        </div>
    </div>
@endsection
