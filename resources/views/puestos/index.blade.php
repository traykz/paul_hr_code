@extends('layouts.app')
@section('content')
<div class="container">
    <h4 class="grey-text text-darken-1 center">Puestos</h4>
    {{-- Search --}}
    <div class="row mb-0">
        <ul class="collapsible">
            <li>
                <div class="collapsible-header">
                    <i class="material-icons">Buscar</i>
                    Buscar Puestos
                </div>
                <div class="collapsible-body">
                    <div class="row mb-0">
                        <form action="{{route('puestos.search')}}" method="POST">
                            @csrf()
                            <div class="input-field col s12 m6 l5 xl6">
                                <input id="search" type="text" name="search" >
                                <label for="search">Buscar puestos</label>
                                <span class="{{$errors->has('search') ? 'helper-text red-text' : '' }}">{{$errors->has('search') ? $errors->first('search') : '' }}</span>
                            </div>
                            <div class="input-field col s12 m6 l4 xl4">
                                <select name="options" id="options">
                                    <option value="nombre">Nombre(s)</option>
                                </select>
                                <label for="options">Buscar por Opciones</label>
                            </div>
                            <br>
                            <div class="col l2">
                                <button type="submit" class="btn waves-effect waves-light">Buscar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </li>
        </ul>
    </div>
    {{-- Search END --}}
        <!-- Show All Employee List as a Card -->
    <div class="card">
        <div class="card-content">
            <div class="row">
                <h5 class="pl-15 grey-text text-darken-2">Lista de Puestos</h5>
                <!-- Table that shows Employee List -->
                <table class="responsive-table col s12 m12 l12 xl12">
                    <thead class="grey-text text-darken-1">
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody id="emp-table">
                        <!-- Check if there are any employee to render in view -->
                        @if($puestos->count())
                            @foreach($puestos as $puesto)
                                <tr>
                                    <td>{{$puesto->id}}</td>
                                    <td>{{$puesto->nombre}}</td>
                                    <td>
                                    <a href="{{route('puestos.edit',$puesto->id)}}" class="btn btn-small btn-floating waves=effect waves-light teal lighten-2"><i class="material-icons">list</i></a>
                                    </td>
                                </tr>
                            @endforeach
                            @if(isset($search))
                                <tr>
                                    <td colspan="4">
                                        <a href="/puestos" class="right">Ver Todos</a>
                                    </td>
                                </tr>
                            @endif
                        @else
                            {{-- if there are no puestos then show this message --}}
                            <tr>
                                <td colspan="5"><h6 class="grey-text text-darken-2 center">No se encontraron registros!</h6></td>
                            </tr>
                        @endif
                    </tbody>
                </table>
                <!-- puestos Table END -->
            </div>
            <!-- Show Pagination Links -->
        <!--    <div class="center">
            $puestos->links('vendor.pagination.default',['paginator' => $puestos])}}
            </div> -->
        </div>
    </div>
    <!-- Card END -->
</div>
<!-- This is the button that is located at the right bottom, that navigates us to puestos.create view -->
<div class="fixed-action-btn">
    <a class="btn-floating btn-large waves=effect waves-light red" href="{{route('puestos.create')}}">
        <i class="large material-icons">add</i>
    </a>
</div>
@endsection
