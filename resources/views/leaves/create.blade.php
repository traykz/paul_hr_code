@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="card col s12 m8 offset-m2 l8 offset-l2 xl8 offset-xl2 card-mt-15">
                <h4 class="center grey-text text-darken-2 card-title">Tipos de Salida</h4>
                <div class="card-content">
                    <div class="row">
                        <!--
                            $errors has all validation errors if you wanna
                            show validation failure message then you can use it
                            like below.
                            $errors->has('input name') will return boolean
                            Request::old('input name') will return the value of the input field
                            that we have submitted.
                            $errors->first('input name') will return the first validation error,
                            so you can show it on the form.
                        -->
                        <form action="{{route('leaves.store')}}" method="POST">
                            <div class="input-field">
                            <i class="material-icons prefix">account_balance</i>                               
                                <input type="text" name="nombre" id="nombre" class="validate" value="{{ Request::old('nombre') ? : '' }}">
                                <label for="nombre">Tipo de Salida</label>                               
                                <span class="{{$errors->has('nombre') ? 'helper-text red-text' : '' }}">{{$errors->first('nombre')}}</span>
                            </div>                          
                            <div class="input-field">
                                <i class="material-icons prefix">account_balance</i>                               
                                    <input type="text" name="dias" id="dias" class="validate" value="{{ Request::old('dias') ? : '' }}">
                                    <label for="dias">Días de Salida</label>                               
                                    <span class="{{$errors->has('dias') ? 'helper-text red-text' : '' }}">{{$errors->first('dias')}}</span>
                                </div>   
                            @csrf()
                            <button type="submit" class="btn waves-effect waves-light mt-15 col s6 offset-s3 m4 offset-m4 l4 offset-l4 xl4-offset-xl4">Crear</button>
                        </form>
                    </div>
                </div>
                <div class="card-action">
                    <a href="/leaves">Regresar</a>
                </div>
            </div
        </div>
    </div>
@endsection