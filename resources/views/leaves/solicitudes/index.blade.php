@extends('layouts.app')

@push('head')

<style>
table.dataTable tbody th,
table.dataTable tbody td {
    white-space: nowrap;
}
    </style>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
<link href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">


@endpush


@section('content')
<div class="container">
    <div class="card">
        <div class="card-content">
        <h5 class="grey-text text-darken-1 center">Solicitudes de mi Equipo</h5>


        @if($tipo === 'boss')
        <div class="row">
                <table width="100%" id="solicitudes" class="table-responsive display ">
                        <thead class="grey-text text-darken-1">
                            <tr>
                                <th>Id</th>
                                <th>Nombre</th>
                                <th>Fechas Solicitadas</th>
                                <th>Cantidad</th>
                                <th>Status</th>
                                <th>Detalles</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>

                        @if($solicitudes_jefe->count())
                           @foreach($solicitudes_jefe as $quien)
                                @foreach($quien as $el)
                            <tr>
                            <td>{{$el->id}}</td>
                            <td>{{$el->user->name}} {{$el->user->last_name}}</td>
                            <td><span class="new badge " data-badge-caption="{{$el->requested_from}}"> </span> - <span class="new badge red " data-badge-caption="{{$el->requested_at}}"></span></td>

                            <td>{{$el->requested_days}}</td>
                            <td>{{$el->status}}</td>
                            <td><button  data-target="modal1"  data-url="{{$el->user->id}}" class="btn modal-trigger">Detalles</button></td>

                            <td>
                                 @if($tipo === 'boss')
                                    @if($el->status == 'pendiente')
                                        <a href="{{route('solicitudes.preaprobar',$el->id)}}" class="btn btn-small btn-floating waves=effect waves-light teal lighten-2"><i class="material-icons">check</i></a>
                                        <a href="{{route('solicitudes.rechazar',$el->id)}}" class="btn btn-small btn-floating waves=effect waves-light red darken-1"><i class="material-icons">close</i></a>
                                    @endif
                                 @elseif($tipo === 'admin' )
                                    @if($el->status == 'preaprobado')
                                        <a href="{{route('solicitudes.aceptar',$el->id)}}" class="btn btn-small btn-floating waves=effect waves-light teal lighten-2"><i class="material-icons">check</i></a>
                                    @endif
                                @else

                                @endif
                </td>
                            </tr>

                            @endforeach
                            @endforeach
                            @endif
                        </tbody>
                 </table>
            </div>  <!-- card row end-->
        @elseif($tipo === 'admin')
        <div class="row">
                <table width="100%" id="solicitudes" class="table-responsive display ">
                        <thead class="grey-text text-darken-1">
                            <tr>
                                <th>Id</th>
                                <th>Nombre</th>
                                <th>Fechas Solicitadas</th>
                                <th>Cantidad</th>
                                <th>Status</th>
                                <th>Detalles</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if($todas->count())
                           @foreach($todas as $quien)
                            <tr>
                            <td>{{$quien->id}}</td>
                            <td>{{$quien->user->name}} {{$quien->user->last_name}}</td>
                            <td><span class="new badge " data-badge-caption="{{$quien->requested_from}}"> </span> - <span class="new badge red " data-badge-caption="{{$quien->requested_at}}"></span></td>
                            <td>{{$quien->requested_days}}</td>
                            <td>{{$quien->status}}</td>
                            <td>  <button    data-target="modal1"   data-url="{{$quien->user->id}}" class="btn modal-trigger">Detalles</button></td>
                            <td>
                                 @if($tipo === 'boss')
                                    @if($quien->status == 'pendiente')
                                        <a href="{{route('solicitudes.preaprobar',$quien->id)}}" class="btn btn-small btn-floating waves=effect waves-light teal lighten-2"><i class="material-icons">check</i></a>
                                        <a href="{{route('solicitudes.rechazar',$quien->id)}}" class="btn btn-small btn-floating waves=effect waves-light red darken-1"><i class="material-icons">close</i></a>
                                    @endif
                                 @elseif($tipo === 'admin' )
                                    @if($quien->status == 'preaprobado')
                                        <a href="{{route('solicitudes.aceptar',$quien->id)}}" class="btn btn-small btn-floating waves=effect waves-light teal lighten-2"><i class="material-icons">check</i></a>
                                    @endif
                                @else

                                @endif
                            </td>
                            </tr>

                            @endforeach
                            @endif
                        </tbody>
                 </table>
            </div>  <!-- card row end-->
        @endif


            </div><!--card Content-->
    </div><!-- Card END -->
</div>

<div class="fixed-action-btn">
    <a class="btn-floating btn-large waves=effect waves-light red" href="{{route('solicitudes.create')}}">
        <i class="large material-icons">add</i>
    </a>
</div>


<!-- Modal Structure
<div id="modal1" class="modal">
    <div class="modal-content">
      <h4>Modal Header</h4>
      <p>A bunch of text</p>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-close waves-effect waves-green btn-flat">Agree</a>
    </div>
  </div> -->


  <div id="modal1" class="modal bottom-sheet">
    <div class="col s6 modal-content">


      <div class="row ">


<div class="col s12 m8 offset-m1 xl3 offset-xl1">


<h5 class="black-text text-darken-2 gray center">Salidas</h5>

                  <div class="collection">
                  <a   href="#!" class="collection-item black-text">Vacaciones Asignadas<span class="new badge " id="asignadas" data-badge-caption="Asignadas"></span></a>
                  </div>

                  <div class="collection">
                    <a   href="#!" class="collection-item black-text">Vacaciones Disponibles<span class="new badge " id="disponibles" data-badge-caption="Disponibles"></span></a>
                    </div>

                 <div class="collection">
                 <a   href="#!" class="collection-item black-text">Vacaciones Usadas<span class="new badge red" id="usadas" data-badge-caption="Usadas"></span></a>
                 </div>
</div>



          <div class="col s12 m8 offset-m1 xl3 offset-xl1">

           <canvas id="employee"></canvas>

          </div>








      </div>
    </div>
    <div class="modal-footer">
      <!--<a href="#!" class="mb-15 modal-close waves-effect waves-red red btn-large btn-floating"><i class="large material-icons">close</i></a>-->
    </div>
  </div>

@push('script')



<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

<script>

$(document).ready( function () {

    $('#solicitudes').DataTable({
        "order": [[ 0, "desc" ]],
        "scrollX": true
    });



    $('.modal-trigger').click(function(){
    var dataUrl;
    var dataUrl = $(this).attr('data-url');
    $.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
     $.ajax({
      type : 'GET',
      url: '{{url("/solicitudes/getSalidas")}}/'+dataUrl,
      dataType: 'json',
      success: function(response)
      {

        console.log(response);
         $("#asignadas").html(response.asignadas);
         $("#disponibles").html(response.disponibles);
         $("#usadas").html(response.usadas);
      },
      error: function(xhr, status, error) {
          console.log("error :");
        }



    });

  });





});


</script>

    {{-- include the chart.js Library --}}
    <script src="{{asset('js/Chart.js')}}"></script>

    {{-- Create the chart with javascript using canvas --}}
    <script>
        // Get Canvas element by its id
        employee_chart = document.getElementById('employee').getContext('2d');
        chart = new Chart(employee_chart,{
            type:'line',
            data:{
                labels:[
                    /*
                        this is blade templating.
                        we are getting the date by specifying the submonth
                     */
                    '{{Carbon\Carbon::now()->subMonths(4)->toFormattedDateString()}}',
                    '{{Carbon\Carbon::now()->subMonths(3)->toFormattedDateString()}}',
                    '{{Carbon\Carbon::now()->subMonths(2)->toFormattedDateString()}}',
                    '{{Carbon\Carbon::now()->subMonths(1)->toFormattedDateString()}}'
                    ],
                datasets:[{
                    label:'Empleados Totales Ultimos 4 Meses',
                    data:[
                        '{{isset($emp_count_4)}})',
                        '{{isset($emp_count_3)}})',
                        '{{isset($emp_count_2)}})',
                        '{{isset($emp_count_1)}})',                    ],
                    backgroundColor: [
                        'rgba(178,235,242 ,1)'
                    ],
                    borderColor: [
                        'rgba(0,150,136 ,1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });
    </script>




@endpush




@endsection
