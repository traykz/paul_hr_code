@extends('layouts.app')
@section('content')
    <div class="container">



        <div class="row">


            <div class="card col card-mt-15">

                <h4 class="center grey-text text-darken-2 card-title">                    <i class="material-icons prefix">event_note</i>
                     Solicitud de Salida  de  {{$has->name}}  {{$has->last_name}}
                    </h4>
                <div class="input-field center ">
                        <h6>Fecha: {{$today}}</h6>
                        </div>




                <div class="card-content">
                    <div class="row">
                         <form action="{{route('solicitudes.store')}}" method="POST">
                            @if($has->dias_vacacion != null)
                            <div class="input-field col s12 m12 l12 xl12">
                                    <i class="material-icons prefix">person_outline</i>
                                <select name="custom_leave">
                                        <option  value="{{$custom_leave->id}}" {{ old('custom_leave')? 'selected' : '' }}>{{$custom_leave->nombre}} ({{$custom_leave->dias}} días)</option>
                                </select>
                                <label for="custom_leave"></label>
                            </div>
                            @endif
                            <div class="input-field col s12 m6 l6 xl6">
                                <i class="material-icons prefix">date_range</i>
                                <input type="text" name="request_from" id="request_from" class="datepicker" value="{{Request::old('request_from') ? : ''}}">
                                <label for="request_from">Desde</label>
                                <span class="{{$errors->has('request_from') ? 'helper-text red-text' : ''}}">{{$errors->has('request_from') ? $errors->first('request_from') : '' }}</span>
                            </div>

                            <div class="input-field col s12 m6 l6 xl6">
                                <i class="material-icons prefix">date_range</i>
                            <input type="text" name="request_at" id="request_at" class="datepicker" value="{{old('request_at') ? : ''}}">
                            <label for="request_at">Hasta</label>
                            <span class="{{$errors->has('request_at') ? 'helper-text red-text' : ''}}">{{$errors->has('request_at') ? $errors->first('request_at') : ''}}</span>

                            </div>



                        <div class="input-field col s12 m12 l12 xl12 ">
                                <i class="material-icons prefix">event_note</i>
                                <textarea name="comentarios" id="comentarios" class="materialize-textarea" >{{Request::old('comentarios') ? : ''}}</textarea>
                                <label for="comentarios">Comentarios</label>
                                <span class="{{$errors->has('comentarios') ? 'helper-text red-text' : ''}}">{{$errors->has('comentarios') ? $errors->first('address') : ''}}</span>
                            </div>



                            @csrf()
                            <button type="submit" class="btn waves-effect waves-light mt-15 col s6 offset-s3 m4 offset-m4 l4 offset-l4 xl4-offset-xl4">Solicitar Salida</button>
                        </form>
                    </div>
                </div>
                <div class="card-action">
                    <a href="/solicitudes">Regresar</a>
                </div>
            </div>


        </div>



            </div>
    </div>
@endsection
