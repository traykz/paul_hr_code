@extends('layouts.app')
@section('content')
<div class="container">

        <!-- Show All Employee List as a Card -->
    <div class="card">
        <div class="card-content">
            <div class="row">
                <h5 class="pl-15 grey-text text-darken-2">Lista de leaves</h5>
                <!-- Table that shows Employee List -->
                <table class="responsive-table col s12 m12 l12 xl12">
                    <thead class="grey-text text-darken-1">
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Dias</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody id="emp-table">
                        <!-- Check if there are any employee to render in view -->
                        @if($leaves->count())
                            @foreach($leaves as $leave)
                                <tr>
                                    <td>{{$leave->id}}</td>
                                    <td>{{$leave->nombre}}</td>
                                      <td>{{$leave->dias}}</td>
                                    <td>
                                    <a href="{{route('leaves.edit',$leave->id)}}" class="btn btn-small btn-floating waves=effect waves-light teal lighten-2"><i class="material-icons">list</i></a>
                                    </td>
                                </tr>
                            @endforeach
                            @if(isset($search))
                                <tr>
                                    <td colspan="4">
                                        <a href="/leaves" class="right">Ver Todos</a>
                                    </td>
                                </tr>
                            @endif
                        @else
                            {{-- if there are no leaves then show this message --}}
                            <tr>
                                <td colspan="5"><h6 class="grey-text text-darken-2 center">No se encontraron registros!</h6></td>
                            </tr>
                        @endif
                    </tbody>
                </table>
                <!-- leaves Table END -->
            </div>
            <!-- Show Pagination Links -->
        <!--    <div class="center">
            $leaves->links('vendor.pagination.default',['paginator' => $leaves])}}
            </div> -->
        </div>
    </div>
    <!-- Card END -->
</div>
<!-- This is the button that is located at the right bottom, that navigates us to leaves.create view -->
<div class="fixed-action-btn">
    <a class="btn-floating btn-large waves=effect waves-light red" href="{{route('leaves.create')}}">
        <i class="large material-icons">add</i>
    </a>
</div>
@endsection
